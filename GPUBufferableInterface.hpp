#ifndef GPU_BUFFERABLE_INTERFACE
#define GPU_BUFFERABLE_INTERFACE
#include "InternalRenderFormat.hpp"
#include "GLBuffer.hpp"                                             
#include "GLPipelineState.hpp"
#include "GLRenderFormat.hpp"
#include "GLShader.hpp"
#include "UniformAttributeInterfaceObject.hpp"


/*Interface provides all common methods used by the renderer to be able to be buffered to gpu*/
class GPUBufferableInterface: public UniformAttributeInterfaceObject{
public:
	/*Gives buffer, expects to retuns size of the data which will be used within the buffer*/
	virtual bool Initialize(GLBuffer* ssbo, GLBuffer* genericBuffer, const unsigned int, const unsigned int) = 0;
	/*Executed every frame, useful for buffering data per draw call*/
	virtual void Update( double frameTime) = 0;
	/*Returns pipeline state used for correct draw of the object */
	virtual GLPipelineState* GetPipelineStateSettings() = 0;
	/*Returns all vertex formats used by interface*/
	virtual void GetAllVertexFormats(std::vector<GLRenderFormat::VertexFormat>& formats) = 0;
	/*Returns memory requirements for ssbo*/
	virtual unsigned int GetReqMemorySSBO() = 0;
	/*Returns memory requirements for generic buffer*/
	virtual unsigned int GetReqMemoryGenericBuffer() = 0;
	/*Returns all bound shaders*/
	virtual void GetShaders(std::vector<GLShader*>& shaders) = 0;
	/*Returns incomplete drawcommand*/
	virtual void GetIncompleteDrawCommand(InternalRenderFormat::IncopleteCommand& comand) = 0;
private:

};


#endif // !GPU_BUFFERABLE_INTERFACE
