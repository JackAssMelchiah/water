#include "FluidSolverFactory.hpp"



GPUBufferableInterface* FluidSolverFactory::NewSolver(TYPE type, const unsigned int size) {
	
	switch (type) {
		case TYPE::CPU_2D:{
			return new BufferableCPUFluid2D(size);
			break;
		}
		case TYPE::CPU_3D:{
			return new BufferableCPUFluid3D(size);
			break;
		}
		case TYPE::GPU_2D:{
			//return new BufferableGPUFluid2D(size);
			return nullptr;
			break;
		}
		default:
		return nullptr;
		break;
	}
}
