#include "InternalRenderFormat.hpp"

bool operator==(const InternalRenderFormat::Metadata& data, const InternalRenderFormat::Metadata& data2) {

	//printf("Address of x is %p\n", (void *)&data);
	return data.id == data2.id;
}


/*Converts IncompleteDrawCommand to DrawCommand*/
void InternalRenderFormat::CompleteDrawCommand(const InternalRenderFormat::IncopleteCommand & ic, InternalRenderFormat::DrawCommand & c, unsigned int baseOffset, unsigned int baseInstance) {

	c.count = ic.count;
	c.instanceCount = ic.instanceCount;
	c.baseInstance = baseInstance;
	c.first = baseOffset;
}

/*TEMPORAL*/
int InternalRenderFormat::DataTypeConsistsOf(DATA_TYPE type) {

	if (type == DATA_TYPE::MAT4) return 16;
	return ((int)type) + 1;
}


/*Frees data in Metadata.data*/
void InternalRenderFormat::FreeMetadata(Metadata& data) {

	if (data.type == InternalRenderFormat::DATA_TYPE::FLOAT) {
		delete static_cast<std::vector<float>*>(data.data);
		return;
	}

	if (data.type == InternalRenderFormat::DATA_TYPE::VEC2) {
		delete static_cast<std::vector<glm::vec2>*>(data.data);
		return;
	}

	if (data.type == InternalRenderFormat::DATA_TYPE::VEC3) {
		delete static_cast<std::vector<glm::vec3>*>(data.data);
		return;
	}

	if (data.type == InternalRenderFormat::DATA_TYPE::VEC4) {
		delete static_cast<std::vector<glm::vec4>*>(data.data);
		return;
	}
}


/*Creates string with readable metadata format*/
std::string InternalRenderFormat::MetadataToString(InternalRenderFormat::Metadata* data) {

	std::string retval;
	retval += "\n\n***id***\t" + std::to_string(data->id);
	retval += "\nbinding point\t"+ std::to_string(data->binding_point);
	retval += "\ndivisor\t\t" + std::to_string(data->divisor);
	retval += "\nlength\t\t" + std::to_string(data->length);
	retval += "\nbytesPerType\t" + std::to_string(DataTypeConsistsOf(data->type));
	retval += "\nmatrix collums\t" + std::to_string(data->numMatrixColls);
	retval += "\n*data*\n";
	float* prt = (float*)data->data;

	for (int i = 0; i < data->length; ++i) {
		retval += std::to_string(*(prt + i));
		retval += " ";
	}
	retval += "\n";
	return retval;
}