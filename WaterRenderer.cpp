#include "WaterRenderer.hpp"


WaterRenderer::WaterRenderer(SDL_Window* w, const unsigned int gridRows):
	waterRows(gridRows + 2), dataBuffer(), ssbo(), uniformBuffer(), uniformBlock(), im(),
	camera(w, glm::vec3(0.f,-2.6f,+8.f), glm::vec3(0.f,0.f,-1.f), 75.f, 16/9.f, glm::vec2(0.1f,1000.f)),
    timer() {

	this->window = w;
	this->camera.ChangeCameraPitch(-20.f);

}


WaterRenderer::~WaterRenderer() {


}


/*Initializes everything*/
bool WaterRenderer::Init(FluidSolverFactory::TYPE type) {

	this->current_type = type;
#ifdef _DEBUG
	EnableDebug();
#endif // _DEBUG

	std::vector<GLRenderFormat::VertexFormat> water_formats;

	//generate vao
	glGenVertexArrays(1, &this->VAO);
	glBindVertexArray(this->VAO);

	//setup addinational gl parameters
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	SDL_GL_SetSwapInterval(0);

	//setup adinational sdl parameters
	glm::ivec2 resolution = Utils::GetWindowResolution(this->window);
	//SDL_SetRelativeMouseMode(SDL_TRUE);
	//SDL_WarpMouseInWindow(this->window, resolution.x / 2, resolution.y / 2);

	//build solver
	this->water = FluidSolverFactory::NewSolver(this->current_type, this->waterRows - 2);

	//check for sizes in buffers, and init them accordingly
	//initialize generic buffer
	if (!this->dataBuffer.Initialize(this->water->GetReqMemoryGenericBuffer())) return false;
	//initialize ssbo
	if (!this->ssbo.Initialize(this->water->GetReqMemorySSBO())) return false;
	//init uniform buffer
	if (!this->uniformBuffer.Initialize(sizeof(glm::mat4) * 2 + sizeof(glm::ivec4))) return false;
	//init draw call buffer
	if (!this->drawCallsBuffer.Initialize(sizeof(InternalRenderFormat::DrawCommand) * 1)) return false;

	//now initialize water
	bool retval = this->water->Initialize(&this->ssbo, &this->dataBuffer, 0, 0);
	assert(retval);
	//querry vertex formats
	this->water->GetAllVertexFormats(water_formats);
	//apply them to VAO
	for (auto format : water_formats) {
		ApplyVertexFormat(format);
	}

	//add all water uniforms to uniform block
	//AddUniform(this->water, this->uniformBlock);
	//setup another uniform block, this one for camera
	AddUniform(this->camera, this->uniformBlock);
	AddUniform(dynamic_cast<UniformAttributeInterfaceObject*>(this->water), this->uniformBlock);

	//get loaded shader 
	std::vector<GLShader*>shaders;
	this->water->GetShaders(shaders);

	this->shader = shaders[0];

	//if def tak add uniform to block ??
	this->uniformBuffer.SetUpShaderBinding(0, this->shader->GetProgram());
	
	//now prepare draw command
	InternalRenderFormat::DrawCommand command;
	InternalRenderFormat::IncopleteCommand command_ic;
	this->water->GetIncompleteDrawCommand(command_ic);
	InternalRenderFormat::CompleteDrawCommand(command_ic, command, 0, 0);
	
	//buffer draw command
	this->drawCallsBuffer.OpenBuffer();
	this->drawCallsBuffer.BufferData(&command, 1, 0);
	this->drawCallsBuffer.CloseBuffer();

	return true;
}


/*Adds new uniform data from object which has them to selected uniform block*/
void WaterRenderer::AddUniform(UniformAttributeInterfaceObject* object, GLUniformBlock& targetBlock) {
	//check if alocated size of the buffer is suffitient
#ifdef _DEBUG
	int allocated_size = this->uniformBuffer.GetByteSize();
	int used_size = this->uniformBuffer.GetFreeMemoryOffset();
	int renferences_size = 0;
	for (int i = 0; i < object->GetReferenceCount(); ++i) {
		renferences_size += object->GetRefSize(i);
	}
	assert(allocated_size >= (renferences_size + used_size));
#endif // _DEBUG

	//parse uniform pointers from object, and add them to uniform block
	for (int i = 0; i < object->GetReferenceCount(); ++i) {
		targetBlock.SetNewDataPointer(object->GetRefPtr(i), object->GetRefSize(i));
	}

}


/*Adds new uniform data from object which has them to selected uniform block*/
void WaterRenderer::AddUniform(UniformAttributeInterfaceObject& object, GLUniformBlock& targetBlock) {

	//check if alocated size of the buffer is suffitient
	#ifdef _DEBUG
	int allocated_size = this->uniformBuffer.GetByteSize();
	int used_size = this->uniformBuffer.GetFreeMemoryOffset();
	int renferences_size = 0;
	for (int i = 0; i < object.GetReferenceCount(); ++i) {
		renferences_size += object.GetRefSize(i);
	}
	assert(allocated_size >= (renferences_size + used_size));
	#endif // _DEBUG

	//parse uniform pointers from object, and add them to uniform block
	for (int i = 0; i < object.GetReferenceCount(); ++i) {
		targetBlock.SetNewDataPointer(object.GetRefPtr(i), object.GetRefSize(i));
	}
}

 
/*Records VAO with vboFormat struct */
void WaterRenderer::ApplyVertexFormat(GLRenderFormat::VertexFormat& format) {
	
	//enable the req attribute
	glEnableVertexArrayAttrib(this->VAO, format.bindPoint);
	//set format for my binding point
	glVertexArrayAttribFormat(this->VAO, format.bindPoint, format.dataConsistOf, GL_FLOAT, GL_FALSE, 0);	//fixed value to float, and offset is 0 due to value not being in any struct
	//bind the vertex format specification saved in vao be used at this binding point -- should be called per shader switch, as attributes in multiple shaders tend to have different formats 																	
	glVertexArrayAttribBinding(this->VAO, format.bindPoint, format.bindPoint);
	//bind the buffer with selected data format - replaces glVertexAttribPointer, also no need to bind buffer first!
	glVertexArrayBindingDivisor(this->VAO, format.bindPoint, format.divisor);
	//apply format to specific buffer | fast switch to it
	glVertexArrayVertexBuffer(this->VAO, format.bindPoint, format.bufferHandle, format.bufferOffset, format.stride); //stride is mult by numMatrixColls for matrix, if its not matrix, its allways 1 
}


/*Clears window*/
void WaterRenderer::Clear() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


/*Swaps window*/
void WaterRenderer::Swap() {

	SDL_GL_SwapWindow(this->window);
}


/*Performs step on cpu version (input, whole step, sends data to gpu), on gpu version just sends prev to gpu, and clears them*/
void WaterRenderer::Update(double frameTime) {

	this->water->Update(frameTime);

	//actualize the uniforms
	this->uniformBuffer.OpenBuffer();
	this->uniformBuffer.BufferData(&this->uniformBlock, 0);
	this->uniformBuffer.CloseBuffer();
}


/*Renders water scene*/
void WaterRenderer::Render() {

	//use shader
	this->shader->UseShader();
	
	unsigned int size = this->water->GetReqMemorySSBO();
	this->ssbo.UseBufferForCompute(1, 0 * size, size);
	//this->ssbo.UseBufferForCompute(2, 1 * size, size);
	//this->ssbo.UseBufferForCompute(3, 2 * size, size);

	this->uniformBuffer.UseBufferForRender(); //throw whole ubo if cpu implementation
	this->drawCallsBuffer.UseBufferForRender();
	this->dataBuffer.UseBufferForRender();

	glBindVertexArray(this->VAO);
	
	glDrawArraysIndirect(this->water->GetPipelineStateSettings()->GetTopologyMode(), nullptr);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glBindVertexArray(0);
		
	this->ssbo.UnuseBufferForCompute();
	this->uniformBuffer.UnuseBufferForRender();
	this->dataBuffer.UnuseBufferForRender();
	this->drawCallsBuffer.UnuseBufferForRender();
}


/*Actualizes input, updates camera*/
WaterRenderer::MAIN_INPUT_TYPE WaterRenderer::HandleInput(std::vector<SDL_Event>& events, double frameTime) {

	//process events from outside, and get them for GUI, tells also if should quit
	if (this->im.ProcessAllInputs(events)) {
		return MAIN_INPUT_TYPE::QUIT;
	}

	Keyboard* keyboard = this->im.GetKeyboard();
	Mouse* mouse = this->im.GetMouse();

	if (keyboard->GetKeyState(SDLK_ESCAPE)) {
		return MAIN_INPUT_TYPE::QUIT;
	}

	//fps cam input
	if (keyboard->GetKeyState(SDLK_w)) {
		this->camera.MoveForwardBackward(+5.f * frameTime);
	}
	else if (keyboard->GetKeyState(SDLK_s)) {
		this->camera.MoveForwardBackward(-5.f * frameTime);
	}
	else if (keyboard->GetKeyState(SDLK_a)) {
		this->camera.StrafeLeftRight(-5.f * frameTime);
	}
	else if (keyboard->GetKeyState(SDLK_d)) {
		this->camera.StrafeLeftRight(+5.f * frameTime);
	}
	else if (keyboard->GetKeyState(SDLK_LCTRL)) {
		this->camera.MoveUpDown(-5.f * frameTime);
	}
	else if (keyboard->GetKeyState(SDLK_SPACE)) {
		this->camera.MoveUpDown(+5.f * frameTime);
	}
	else if (keyboard->GetKeyState(SDLK_m)) {
		//this->water.SaveSerialize();
	}
	else if (keyboard->GetKeyState(SDLK_l)) {
		//this->water.LoadSerialized();
		DebugGPUOutputs();
	}
	else if (keyboard->GetKeyState(SDLK_b)) {
		DebugCPUOutputs();
	}


	glm::vec2 mousecoords = mouse->GetMouseMotionAcceleration();
	Mouse::MouseWheel wheelState = mouse->GetMouseWheelState();

	this->camera.ChangeCameraYaw(mousecoords.x * 5 * frameTime);
	this->camera.ChangeCameraPitch(mousecoords.y * 5 * frameTime);

	Mouse::MousePress mousePressL = mouse->GetMouseButtonState(Mouse::MouseButton::BUTTON_LEFT);
	Mouse::MousePress mousePressR = mouse->GetMouseButtonState(Mouse::MouseButton::BUTTON_RIGHT);

	if (mousePressL.press) {
		if (this->current_type == FluidSolverFactory::TYPE::CPU_2D) {
			auto fluid = dynamic_cast<CPUFluidSolver2D*>(this->water);
			//for (size_t i = 0; i < this->waterRows; ++i)
			fluid->AddForce(glm::ivec2(this->waterRows / 2), 100.f); //was 1000.f
		}
		else {
			auto fluid = dynamic_cast<CPUFluidSolver3D*>(this->water);
			//for (size_t i = 0; i < this->waterRows; ++i)
			fluid->AddForce(glm::ivec3(this->waterRows / 2), 100.f); //was 1000.f
		}
	}
	if (mousePressR.press) {
		if (this->current_type == FluidSolverFactory::TYPE::CPU_2D) {
			auto fluid = dynamic_cast<CPUFluidSolver2D*>(this->water);
			//for (size_t i = 0; i < this->waterRows; ++i)
			fluid->AddDensity(glm::ivec2(this->waterRows / 2), 10.f);
		}
		else {
			auto fluid = dynamic_cast<CPUFluidSolver3D*>(this->water);
			//for (size_t i = 0; i < this->waterRows; ++i)
			fluid->AddDensity(glm::ivec3(this->waterRows / 2), 10.f);
		}
	}
	this->camera.UpdateCamera();
	return MAIN_INPUT_TYPE::NONE;
}


/*Actualizes time*/
void WaterRenderer::HandleTime(double &frameTime) {

	//timers
	frameTime = this->timer.GetTime() * 0.001;
	if (frameTime < 0.00001) { //if some err
		frameTime = 0.0166;
	}
	this->timer.Stop();
	this->timer.Start();
}


/*Main rendering loop*/
void WaterRenderer::Loop() {

	int width, height;
	std::vector<SDL_Event> events;
	double frameTime = 0.;

	MAIN_INPUT_TYPE event_type = MAIN_INPUT_TYPE::NONE;
	
	while (event_type != MAIN_INPUT_TYPE::QUIT) {

		HandleTime(frameTime);
		event_type = HandleInput(events, frameTime);
		Update(frameTime);

		#ifdef GPU_WATER_IMPLEMENTATION
		Compute();
		#endif // GPU_WATER_IMPLEMENTATION

		Clear();
		Render();
		Swap();
	}
}


/*Used from https://github.com/dormon/FitGL */
void WaterRenderer::EnableDebug() {

	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, NULL, GL_FALSE);
	glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_PERFORMANCE, GL_DONT_CARE, 0, NULL, GL_FALSE);
	glDebugMessageCallback((GLDEBUGPROC)(WaterRenderer::DefaultDebugMessage), NULL);
}


/*Used from https://github.com/dormon/FitGL */
void WaterRenderer::DefaultDebugMessage(
	GLenum        source,
	GLenum        type,
	GLuint        /*id*/,
	GLenum        severity,
	GLsizei       /*length*/,
	const GLchar *message,
	void*        /*userParam*/) {
	Utils::PrintOut(
		"source: " + TranslateDebugSource(source) +
		" type: " + TranslateDebugType(type) +
		" severity: " + TranslateDebugSeverity(severity) +
		" : " + message);
}


/*Used from https://github.com/dormon/FitGL */
std::string WaterRenderer::TranslateDebugSource(GLenum source) {
	switch (source) {//swich over debug sources
	case GL_DEBUG_SOURCE_API:return"GL_DEBUG_SOURCE_API";
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:return"GL_DEBUG_SOURCE_WINDOW_SYSTEM";
	case GL_DEBUG_SOURCE_SHADER_COMPILER:return"GL_DEBUG_SOURCE_SHADER_COMPILER";
	case GL_DEBUG_SOURCE_THIRD_PARTY:return"GL_DEBUG_SOURCE_THIRD_PARTY";
	case GL_DEBUG_SOURCE_APPLICATION:return"GL_DEBUG_SOURCE_APPLICATION";
	case GL_DEBUG_SOURCE_OTHER:return"GL_DEBUG_SOURCE_OTHER";
	default:return"unknown";
	}
}


/*Used from https://github.com/dormon/FitGL */
std::string WaterRenderer::TranslateDebugType(GLenum type) {
	switch (type) {//switch over debug types
	case GL_DEBUG_TYPE_ERROR:return"GL_DEBUG_TYPE_ERROR";
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:return"GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:return"GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
	case GL_DEBUG_TYPE_PORTABILITY:return"GL_DEBUG_TYPE_PORTABILITY";
	case GL_DEBUG_TYPE_PERFORMANCE:return"GL_DEBUG_TYPE_PERFORMANCE";
	case GL_DEBUG_TYPE_MARKER:return"GL_DEBUG_TYPE_MARKER";
	case GL_DEBUG_TYPE_PUSH_GROUP:return"GL_DEBUG_TYPE_PUSH_GROUP";
	case GL_DEBUG_TYPE_POP_GROUP:return"GL_DEBUG_TYPE_POP_GROUP";
	case GL_DEBUG_TYPE_OTHER:return"GL_DEBUG_TYPE_OTHER";
	case GL_DONT_CARE:return"GL_DONT_CARE";
	default:return"unknown";
	}
}


/*Used from https://github.com/dormon/FitGL */
std::string WaterRenderer::TranslateDebugSeverity(GLenum severity) {
	switch (severity) {//switch over debug severities
	case GL_DEBUG_SEVERITY_LOW:return"GL_DEBUG_SEVERITY_LOW";
	case GL_DEBUG_SEVERITY_MEDIUM:return"GL_DEBUG_SEVERITY_MEDIUM";
	case GL_DEBUG_SEVERITY_HIGH:return"GL_DEBUG_SEVERITY_HIGH";
	case GL_DEBUG_SEVERITY_NOTIFICATION:return"GL_DEBUG_SEVERITY_NOTIFICATION";
	case GL_DONT_CARE:return"GL_DONT_CARE";
	default:return"unknown";
	}
}


/*Outputs buffers from GPU*/
void WaterRenderer::DebugGPUOutputs() {

	//cast water 
	CPUFluidSolver3D* fluid = dynamic_cast<CPUFluidSolver3D*>(this->water);

	int ssbo_size = this->waterRows * this->waterRows * sizeof(float);
	std::vector<float> out(this->waterRows * this->waterRows);

	this->ssbo.OpenBuffer();

	void* ssbo_buffer_start = this->ssbo.GetBufferMemoryToRead(0);
	Utils::PrintOut("Buffers on GPU:\n");
	//0
	memcpy(out.data(), (char*)ssbo_buffer_start + 0 * ssbo_size, ssbo_size);
	Utils::PrintOut("***ATTR:0***\n");
	fluid->ULTIMATEDEBUG(out.data());
	
	this->ssbo.CloseBuffer();
}


/*Outputs buffers from CPU*/
void WaterRenderer::DebugCPUOutputs() {

	//cast water 
	CPUFluidSolver3D* fluid = dynamic_cast<CPUFluidSolver3D*>(this->water);

	Utils::PrintOut("Buffers on CPU:\n");
	//0
	//Utils::PrintOut("***ATTR:0***\n");
	//fluid->ULTIMATEDEBUG(fluid->GetVelocityX());
	////1
	//Utils::PrintOut("***ATTR:1***\n");
	//fluid->ULTIMATEDEBUG(fluid->GetVelocityY());
	//2
	Utils::PrintOut("***ATTR:2***\n");
	fluid->ULTIMATEDEBUG(fluid->GetDensity());

}