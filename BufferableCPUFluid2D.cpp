#include "BufferableCPUFluid2D.hpp"


BufferableCPUFluid2D::BufferableCPUFluid2D(const unsigned int dimmension):CPUFluidSolver2D(dimmension) {

	SetUniformReferences();
}


BufferableCPUFluid2D::~BufferableCPUFluid2D() {

	//delete this->pipelineState;
}


/*Returns memory requirements for ssbo*/
unsigned int BufferableCPUFluid2D::GetReqMemorySSBO() {

	return (this->gridSize + 2) * (this->gridSize + 2) * sizeof(float);
}


/*Returns memory requirements for generic buffer*/
unsigned int BufferableCPUFluid2D::GetReqMemoryGenericBuffer() {

	return (this->gridSize + 2) * (this->gridSize + 2) * 2 * 3 * sizeof(glm::vec3);
}


/*TReturns incomplete draw command*/
void BufferableCPUFluid2D::GetIncompleteDrawCommand(InternalRenderFormat::IncopleteCommand & command) {

	command.instanceCount = 1;
	command.count = (this->gridSize + 2) * (this->gridSize + 2) * 6;
}


/*Bulds pipeline, with which its able to be rendered with*/
void BufferableCPUFluid2D::CreatePipelineState() {

	//pipeline build
	//this->pipelineState = RendererFactory::NewPipeline();
	
	//build pipeline params
	this->pipelineState.SetDefaultState();
	this->pipelineState.SetFrontFace(PipelineState::FRONT_FACE::CLOCKWISE);
	this->pipelineState.SetPolygonMode(PipelineState::POLYGON_MODE::WIREFRAME);
	this->pipelineState.Build();
}



/*Initialization, buffers grid data to gpu and prepares pipeline */
bool BufferableCPUFluid2D::Initialize(GLBuffer* ssbo, GLBuffer* genericBuffer, const unsigned int ssboOffset, const unsigned int genericOffset) {
	
	this->boundSSBO = ssbo;
	this->boundSSBOOffset = ssboOffset;

	this->boundVBO = genericBuffer;
	this->boundVBOOffset = genericOffset;

	Init(); //initialize simulation part

	//initialize pipeline state
	CreatePipelineState();
	
	//create grid
	std::vector<glm::vec3> grid;
	Shape::Grid(grid, this->gridSize + 2);

	//one time buffering of the grid 
	genericBuffer->OpenBuffer();
	genericBuffer->BufferData(&grid.data()->x, genericOffset, grid.size() * sizeof(glm::vec3));
	genericBuffer->CloseBuffer();

	//no need to buffer ssbo params, as it implicitly starts with 0

	//load shaders
	return CreateShaders();

}


/*Ticks every frame, must upload new data*/
void BufferableCPUFluid2D::Update(double frameTime) {

	unsigned int size = (this->gridSize + 2) * (this->gridSize + 2) * sizeof(float);
	unsigned int attrib_start_offset = boundSSBOOffset;

	CPUFluidSolver2D::Update(frameTime); //updates the water simulation

	this->boundSSBO->OpenBuffer();
	this->boundSSBO->BufferData(this->GetDensity(), attrib_start_offset, size);
	this->boundSSBO->CloseBuffer();
}


/*Returns format of the water grid*/
void BufferableCPUFluid2D::GetAllVertexFormats(std::vector<GLRenderFormat::VertexFormat>& formats) {

	formats.clear();

	GLRenderFormat::VertexFormat format;
	format.bufferHandle = this->boundVBO->GetBufferHandle();
	format.bindPoint = 0;
	format.dataConsistOf = 3;
	format.bufferOffset = 0; //boundVBOOffset
	format.stride = 3 * sizeof(float);
	format.divisor = 0;
	
	formats.push_back(format);
}


/*Returns pipeline state*/
GLPipelineState* BufferableCPUFluid2D::GetPipelineStateSettings() {
	
	return &this->pipelineState;
}


/*Returns all used shaders*/
void BufferableCPUFluid2D::GetShaders(std::vector<GLShader*>& shaders) {

	shaders.clear();
	shaders.push_back(this->boundShader);
}


/*Load all shaders used within this class*/
bool BufferableCPUFluid2D::CreateShaders() {

	this->boundShader = new GLShader();

	if (!this->boundShader->LoadShader(std::string(SHADER_DIR) + "fluid2D.vert", std::string(SHADER_DIR) + "fluid2D.frag")) return false;
	return true;
}


/*Saves references to uniforms*/
void BufferableCPUFluid2D::SetUniformReferences() {

	//does not use uniforms
}


