#include "Texture.hpp"

int Texture::_TEXTURE_COUNT_ = 0;


Texture::Texture(std::string path) {
	
	this->id = Texture::_TEXTURE_COUNT_;
	Texture::_TEXTURE_COUNT_++;
	this->textureName = path;
	glCreateTextures(GL_TEXTURE_2D, 1, &this->textureId);
	//LoadTexture();
}


Texture::~Texture() {

	glDeleteTextures(1, &this->textureId);
}



void Texture::SetTextureVariable(std::string var) {

	this->varName = var;
}


/*Will send image to gpu to do as set up*/
void Texture::DrawImage(GLuint shaderProgram) {

	glActiveTexture(GL_TEXTURE0 + this->id);//use texture unit 0
	glBindTexture(GL_TEXTURE_2D, this->textureId); //set it to be generated texture to be TEX2D
	GLuint SamplerID = glGetUniformLocation(shaderProgram, this->varName.c_str());
	glUniform1i(SamplerID, this->id);
	

}


/*Will send to gpu, for image store/*/
void Texture::DrawDiscreteImage(GLuint shaderProgram) {

	GLuint uniformID = glGetUniformLocation(shaderProgram, this->varName.c_str());
	glUniform1i(uniformID, this->id+1);
	glBindImageTexture(this->id + 1, this->textureId, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA8);
}



Texture::IDTEXTURE Texture::GetID() {

	return this->textureId;
}



glm::ivec2 Texture::GetDimensions() {

	return this->dimmensions;
}



GLuint Texture::GetTextureID() {

	return this->textureId;
}


std::string Texture::GetTextureName() {

	return this->textureName;
}


/*Creates brand new empty texure for read/write in shaders*/
void Texture::NewTexture(glm::ivec2 dimmensions) {

	glTextureStorage2D(this->textureId, 1, GL_RGBA8, dimmensions.x, dimmensions.y);
	this->dimmensions = dimmensions;
}



/*Method loads texture, and buffers it to GPU, with specific texture*/
bool Texture::LoadTexture() {

	SDL_Surface* surface;
	int internal_format = GL_RGB; // for bmp//GL_ALPHA, GL_LUMINANCE, GL_LUMINANCE_ALPHA, GL_RGB, GL_RGBA.
	int texel_format = GL_BGR; //for bmp

	surface = IMG_Load(this->textureName.c_str());
	if (surface == NULL) {
		return false;
	}

	//glBindTexture(GL_TEXTURE_2D, this->textureId);

	//check if its jpg
	for (size_t i = 0; i < this->textureName.size(); ++i) {
		if (this->textureName[i] == '.') {
			switch (this->textureName[i + 1]) {
			case 'j': texel_format = GL_RGB;
			default:
				break;
			}
			break;
		}
	}

	//check for png
	if (surface->format->BytesPerPixel == 4) { //then its png
		internal_format = GL_RGBA;
		texel_format = GL_RGBA;
	}

	//else its bmp

	//allocate textures space
	glTextureStorage2D(this->textureId, 1, GL_RGBA8, surface->w, surface->h); //1 - only one mitmap

	//buffer to previously allocated space
	glTextureSubImage2D(
		this->textureId,
		0,
		0,
		0,
		surface->w,
		surface->h,
		texel_format,
		GL_UNSIGNED_BYTE,
		surface->pixels
	);


	glTextureParameteri(this->textureId, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTextureParameteri(this->textureId, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTextureParameteri(this->textureId, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTextureParameteri(this->textureId, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateTextureMipmap(this->textureId);

	this->dimmensions = glm::ivec2(surface->w, surface->h);

	SDL_FreeSurface(surface);

	return true;
}