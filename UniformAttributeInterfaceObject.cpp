#include "UniformAttributeInterfaceObject.hpp"

/*In constructor the SetUniformReferences is allways called*/
UniformAttributeInterfaceObject::UniformAttributeInterfaceObject() {

	//SetUniformReferences(); NEMUZU KRAKOVSKY PROSTE TO NENDE
}


UniformAttributeInterfaceObject::~UniformAttributeInterfaceObject() {

}


/*Returns how many references the object has for uniforms, eq how many uniforms it has*/
int UniformAttributeInterfaceObject::GetReferenceCount() const {

	return this->references.size();
}


/*Returns data pointer from 0..GetReferenceCount*/
void * UniformAttributeInterfaceObject::GetRefPtr(int i) const {

	return this->references[i].second;
}


/*Returns data size from 0..GetReferenceCount*/
int UniformAttributeInterfaceObject::GetRefSize(int i) const {
	
	return this->references[i].first;
}


/*Saves pointer onto the resource, should be done when resource is defined*/
void UniformAttributeInterfaceObject::SaveReference(void *ptr, int size) {

	this->references.push_back(std::pair<int, void*>(size, ptr));
}
