#include "GLShader.hpp"



/*Cstructor*/
GLShader::GLShader():Shader() {
	
}


/*Dstructor*/
GLShader::~GLShader() {

	if (IsShaderLoaded()) {
		glDeleteProgram(this->programID);
	}
}


/*Loads and creates shader program from compute shader*/
bool GLShader::LoadShader(string computeShaderPath) {

	std::vector<std::vector<char>> source_codes(1);
	GLint result = GL_FALSE;
	GLuint computeShaderID = LoadShaders(computeShaderPath, ShaderType::COMPUTE, &source_codes[0]);

	if (source_codes[0].size() == 0) { return false; }

	//attach and link
	this->programID = glCreateProgram();
	glAttachShader(this->programID, computeShaderID);
	glLinkProgram(this->programID);

	// Check the program
	glGetProgramiv(this->programID, GL_LINK_STATUS, &result);
	if (result == GL_FALSE) {
		PrintProgramLog(this->programID);
		return false;
	}
	//dispose of rest unwanted things
	glDeleteShader(computeShaderID);

	SetShaderLoaded();
	return true;
}


/*Loads andf creates shader program from ver shader*/
bool GLShader::LoadShader(string vertexShaderPath, string fragmentShaderPath) {

	std::vector<std::vector<char>> source_codes(2);
	
	GLint result = GL_FALSE;
	GLuint vertexShaderID = LoadShaders(vertexShaderPath, ShaderType::VERTEX, &source_codes[0]);
	if (source_codes[0].size() == 0) { return false; }
	GLuint fragmentShaderID = LoadShaders(fragmentShaderPath, ShaderType::FRAGMENT, &source_codes[1]);
	if (source_codes[1].size() == 0) { return false; }


	//attach and link
	this->programID = glCreateProgram();
	glAttachShader(this->programID, vertexShaderID);
	glAttachShader(this->programID, fragmentShaderID);
	glLinkProgram(this->programID);

	// Check the program
	glGetProgramiv(this->programID, GL_LINK_STATUS, &result);
	if (result == GL_FALSE) {
		PrintProgramLog(this->programID);
		return false;
	}
	PrintProgramLog(this->programID);

	//dispose of rest unwanted things
	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

	SetShaderLoaded();
	return true;
}


/**Method loads vertex, geometry and fragment shaders, and compiles them into program**/
bool GLShader::LoadShader(string vertexShaderPath, string geometryShaderPath, string fragmentShaderPath) {

	std::vector<std::vector<char>> source_codes(3);

	GLint result = GL_FALSE;
	GLuint vertexShaderID = LoadShaders(vertexShaderPath, ShaderType::VERTEX, &source_codes[0]);
	if (source_codes[0].size() == 0) { return false; }
	GLuint geometryShaderID = LoadShaders(geometryShaderPath, ShaderType::GEOMETRY, &source_codes[1]);
	if (source_codes[1].size() == 0) { return false; }
	GLuint fragmentShaderID = LoadShaders(fragmentShaderPath, ShaderType::FRAGMENT, &source_codes[2]);
	if (source_codes[2].size() == 0) { return false; }


	//attach and link
	this->programID = glCreateProgram();
	glAttachShader(this->programID, vertexShaderID);
	glAttachShader(this->programID, geometryShaderID);
	glAttachShader(this->programID, fragmentShaderID);
	glLinkProgram(this->programID);

	// Check the program
	glGetProgramiv(this->programID, GL_LINK_STATUS, &result);
	if (result == GL_FALSE) {
		PrintProgramLog(this->programID);
		return false;
	}
	//dispose of rest unwanted things
	glDeleteShader(vertexShaderID);
	glDeleteShader(geometryShaderID);
	glDeleteShader(fragmentShaderID);

	SetShaderLoaded();
	return true;
}


/**Method loads vertex, tesalation controll, tesalation evaluation and fragment shaders, and compiles them into program**/
bool GLShader::LoadShader(string vertexShaderPath, string tessalationControllShaderPath, string tessalationEvaluationShaderPath, string fragmentShaderPath) {

	std::vector<std::vector<char>> source_codes(4);
	GLint result = GL_FALSE;
	GLuint vertexShaderID = LoadShaders(vertexShaderPath, ShaderType::VERTEX, &source_codes[0]);
	if (source_codes[0].size() == 0) { return false; }
	GLuint t_controlShaderID = LoadShaders(tessalationControllShaderPath, ShaderType::T_CONTROL, &source_codes[1]);
	if (source_codes[1].size() == 0) { return false; }
	GLuint t_evaluationShaderID = LoadShaders(tessalationEvaluationShaderPath, ShaderType::T_EVALUATUION, &source_codes[2]);
	if (source_codes[2].size() == 0) { return false; }
	GLuint fragmentShaderID = LoadShaders(fragmentShaderPath, ShaderType::FRAGMENT, &source_codes[3]);
	if (source_codes[3].size() == 0) { return false; }


	//attach and link
	this->programID = glCreateProgram();
	glAttachShader(this->programID, vertexShaderID);
	glAttachShader(this->programID, t_controlShaderID);
	glAttachShader(this->programID, t_evaluationShaderID);
	glAttachShader(this->programID, fragmentShaderID);
	glLinkProgram(this->programID);

	// Check the program
	glGetProgramiv(this->programID, GL_LINK_STATUS, &result);
	if (result == GL_FALSE) {
		PrintProgramLog(this->programID);
		return false;
	}
	//dispose of rest unwanted things
	glDeleteShader(vertexShaderID);
	glDeleteShader(t_controlShaderID);
	glDeleteShader(t_evaluationShaderID);
	glDeleteShader(fragmentShaderID);

	SetShaderLoaded();
	return true;
}


/**Method loads vertex, tesalation controll, tesalation evaluation, geometry and fragment shaders, and compiles them into program**/
bool GLShader::LoadShader(string vertexShaderPath, string tessalationControllShaderPath, string tessalationEvaluationShaderPath, string geometryShaderPath, string fragmentShaderPath) {

	std::vector<std::vector<char>> source_codes(5);
	GLint result = GL_FALSE;
	GLuint vertexShaderID = LoadShaders(vertexShaderPath, ShaderType::VERTEX, &source_codes[0]);
	if (source_codes[0].size() == 0) { return false; }
	GLuint t_controlShaderID = LoadShaders(tessalationControllShaderPath, ShaderType::T_CONTROL, &source_codes[1]);
	if (source_codes[1].size() == 0) { return false; }
	GLuint t_evaluationShaderID = LoadShaders(tessalationEvaluationShaderPath, ShaderType::T_EVALUATUION, &source_codes[2]);
	if (source_codes[2].size() == 0) { return false; }
	GLuint geometryShaderID = LoadShaders(geometryShaderPath, ShaderType::GEOMETRY, &source_codes[3]);
	if (source_codes[3].size() == 0) { return false; }
	GLuint fragmentShaderID = LoadShaders(fragmentShaderPath, ShaderType::FRAGMENT, &source_codes[4]);
	if (source_codes[4].size() == 0) { return false; }


	//attach and link
	this->programID = glCreateProgram();
	glAttachShader(this->programID, vertexShaderID);
	glAttachShader(this->programID, t_controlShaderID);
	glAttachShader(this->programID, t_evaluationShaderID);
	glAttachShader(this->programID, geometryShaderID);
	glAttachShader(this->programID, fragmentShaderID);
	glLinkProgram(this->programID);

	// Check the program
	glGetProgramiv(this->programID, GL_LINK_STATUS, &result);
	if (result == GL_FALSE) {
		PrintProgramLog(this->programID);
		return false;
	}
	//dispose of rest unwanted things
	glDeleteShader(vertexShaderID);
	glDeleteShader(t_controlShaderID);
	glDeleteShader(t_evaluationShaderID);
	glDeleteShader(geometryShaderID);
	glDeleteShader(fragmentShaderID);

	SetShaderLoaded();
	return true;
}


/*Method compiles shaders(VERTEX || FRAGMENT), returns handler to them*/ //POSSIBLE STATIC
GLuint GLShader::LoadShaders(string path, ShaderType type, std::vector<char>* src_code) {

	GLuint shaderID;
	// Create the shaders
	if (type == ShaderType::VERTEX) {
		shaderID = glCreateShader(GL_VERTEX_SHADER);
	}

	else if (type == ShaderType::GEOMETRY) {
		shaderID = glCreateShader(GL_GEOMETRY_SHADER);
	}

	else if (type == ShaderType::FRAGMENT) {
		shaderID = glCreateShader(GL_FRAGMENT_SHADER);
	}

	else if (type == ShaderType::COMPUTE) {
		shaderID = glCreateShader(GL_COMPUTE_SHADER);
	}

	else if (type == ShaderType::T_CONTROL) {
		shaderID = glCreateShader(GL_TESS_CONTROL_SHADER);
	}
	else if (type == ShaderType::T_EVALUATUION) {
		shaderID = glCreateShader(GL_TESS_EVALUATION_SHADER);
	}

	if (!ShaderFromFile(path, src_code)) {
		Utils::PrintOutErr("Incorrect path to shader!");
		return -1;
	}

	GLint Result = GL_FALSE;
	const GLchar* const s = (const GLchar*)src_code->data();
	std::vector<GLint> length(1, src_code->size());
	//const GLchar** const s = (const GLchar**)&(src_code->data()[0]);

	glShaderSource(shaderID, 1, &s, length.data());
	glCompileShader(shaderID);

	// Check shader
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &Result);
	//if (Result == GL_FALSE) {
		PrintfShaderLog(shaderID);
	//}

	return shaderID;
}


/**Method gets the program log, and prints it**/
void GLShader::PrintfShaderLog(GLuint shader) {

	if (glIsShader(shader)) {
		int infoLogLength = 0;
		int maxLength = infoLogLength;

		//Get info string length
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		//Allocate string
		char* infoLog = new char[maxLength];

		//Get info log
		glGetShaderInfoLog(shader, maxLength, &infoLogLength, infoLog);
		if (infoLogLength > 0) {
			Utils::PrintOutWar(infoLog);
		}
	}
}


/*Method gets the shader log, and prints it*/ //POSSIBLE STATIC
void GLShader::PrintProgramLog(GLuint program) {

	if (!glIsProgram(program)) {
		return;
	}

	int infoLogLength = 0;
	int maxLength = infoLogLength;

	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

	char* infoLog = new char[maxLength];

	glGetProgramInfoLog(program, maxLength, &infoLogLength, infoLog);
	if (infoLogLength > 0) {
		Utils::PrintOutWar(infoLog);
	}
}


/*Call first when about to render scene*/
void GLShader::UseShader() const {

	glUseProgram(this->programID);
}


/*Getter for programID*/
GLuint GLShader::GetProgram() const {

	return this->programID;

}