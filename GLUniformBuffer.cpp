#include "GLUniformBuffer.hpp"

/*Uniform Buffers*/
GLUniformBuffer::GLUniformBuffer() {

}

GLUniformBuffer::~GLUniformBuffer() {

}


/*Bind as ssbo part of the buffer*/
void GLUniformBuffer::UseBufferForRender(unsigned int offset, unsigned int size) const {

	assert(this->bindPoint.has_value());
	assert(this->shaderProgram.has_value());

	//describes range, which will be "bound" as uniform buffer 
	glBindBufferRange(GL_UNIFORM_BUFFER, this->bindPoint.value(), this->handle, offset, size);

	//classic binding
	glBindBuffer(GL_UNIFORM_BUFFER, this->handle);
}


/*Bind as ssbo whole buffer*/
void GLUniformBuffer::UseBufferForRender() const {

	assert(this->bindPoint.has_value());
	assert(this->shaderProgram.has_value());

	//describes range, which will be "bound" as uniform buffer 
	glBindBufferRange(GL_UNIFORM_BUFFER, this->bindPoint.value(), this->handle, 0, this->byteSize);
	   
	//classic binding
	glBindBuffer(GL_UNIFORM_BUFFER, this->handle);
}


/*Clears bind for ssbo*/
void GLUniformBuffer::UnuseBufferForRender() const{

	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}


/*Sets up binding point which will be then used in UseBufferForRender()*/
void GLUniformBuffer::SetUpShaderBinding( GLuint bindingPoint, GLuint shaderProgram){

	this->bindPoint = bindingPoint;
	this->shaderProgram = shaderProgram;
}


/*Will buffer each data in uniform block in buffer on offset */
void GLUniformBuffer::BufferData(GLUniformBlock* block, int globalOffset) {

	InternalRenderFormat::UniformBlockMetadata* current_uniform;

	assert(GetByteSize() >= block->GetBlockSize() + globalOffset);

	//move end buffer if necessary
	int req_byte_space = block->GetBlockSize() + globalOffset;
	if (req_byte_space > this->freeMemoryPrt) {
		this->freeMemoryPrt = req_byte_space;
	}

	void* dest_ptr = ((char*)this->data) + globalOffset;
	
	for (int i = 0; i < block->GetUniformCount(); ++i) {

		memcpy(dest_ptr, block->GetUniform(i).dataPtr, block->GetUniform(i).byteSize);
		dest_ptr = ((char*)dest_ptr) + block->GetUniform(i).byteSize;
	}
}


