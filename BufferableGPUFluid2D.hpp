#ifndef BUFFERABLE_GPU_FLUID_2D
#define BUFFERABLE_GPU_FLUID_2D

#include "GPUBufferableInterface.hpp"


class BufferableGPUFluid2D: public GPUBufferableInterface {
public:
	BufferableGPUFluid2D();
	virtual ~BufferableGPUFluid2D();

private:

};
#endif // !BUFFERABLE_GPU_FLUID_2D