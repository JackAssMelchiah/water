#ifndef GL_VERTEX_FORMAT_HPP
#define GL_VERTEX_FORMAT_HPP

#include "GL/glew.h"
#include <string>

namespace GLRenderFormat {

	typedef struct {
		GLuint bufferHandle;
		GLuint bindPoint;
		unsigned int dataConsistOf;
		unsigned int bufferOffset;
		unsigned int stride;
		unsigned int divisor;
	}VertexFormat;

	typedef struct {
		unsigned int drawCommandOffsetBytes;
		unsigned int drawCommandCount;
	}DrawCallFormat;

	static std::string VertexFormatToString(GLRenderFormat::VertexFormat* format) {
		std::string s;
		s += "glenablevertexarrayattrib(vao," + std::to_string(format->bindPoint) + ",);\n";
		s += "glvertexarrayattribformat(vao," + std::to_string(format->bindPoint) + ", " + std::to_string(format->dataConsistOf) + ", gl_float, gl_false, 0);\n";
		s += "glvertexarrayattribbinding(vao," + std::to_string(format->bindPoint) + ", " + std::to_string(format->bindPoint) + ");\n";
		s += "glvertexarraybindingdivisor(vao," + std::to_string(format->bindPoint) + ", " + std::to_string(format->divisor) + ");\n";
		s += "glvertexarrayvertexbuffer(vao," + std::to_string(format->bindPoint) + ", " + std::to_string(format->bufferHandle) + ", " + std::to_string(format->bufferOffset) + ", " + std::to_string(format->stride) + ");\n";
		return s;
	}
}



#endif // !GL_VERTEX_FORMAT_HPP
