#include "CPUFluidSolver3D.hpp"

#define INDEX(i, j, k) ((i) + (this->gridSize + 2) * (j) + (this->gridSize + 2) * (this->gridSize + 2) * (k)) 

CPUFluidSolver3D::CPUFluidSolver3D(const unsigned int gridSize):gridSize(gridSize){

	//allocation
	this->uPrev = new float[(gridSize + 2)*(gridSize + 2)*(gridSize + 2)];
	this->vPrev = new float[(gridSize + 2)*(gridSize + 2)*(gridSize + 2)];
	this->wPrev = new float[(gridSize + 2)*(gridSize + 2)*(gridSize + 2)];
	this->densityPrev = new float[(gridSize + 2)*(gridSize + 2)*(gridSize + 2)];

	memset(this->uPrev, 0, (gridSize + 2)*(gridSize + 2)*(gridSize + 2) * sizeof(float));
	memset(this->vPrev, 0, (gridSize + 2)*(gridSize + 2)*(gridSize + 2) * sizeof(float));
	memset(this->wPrev, 0, (gridSize + 2)*(gridSize + 2)*(gridSize + 2) * sizeof(float));
	memset(this->densityPrev, 0, (gridSize + 2)*(gridSize + 2)*(gridSize + 2) * sizeof(float));

	this->density = new float[(gridSize + 2)*(gridSize + 2)*(gridSize + 2)];
	memset(this->density, 0, (gridSize + 2)*(gridSize + 2)*(gridSize + 2) * sizeof(float));

	this->u = new float[(gridSize + 2)*(gridSize + 2)*(gridSize + 2)];
	this->v = new float[(gridSize + 2)*(gridSize + 2)*(gridSize + 2)];
	this->w = new float[(gridSize + 2)*(gridSize + 2)*(gridSize + 2)];

	memset(this->u, 0, (gridSize + 2)*(gridSize + 2)*(gridSize + 2) * sizeof(float));
	memset(this->v, 0, (gridSize + 2)*(gridSize + 2)*(gridSize + 2) * sizeof(float));
	memset(this->w, 0, (gridSize + 2)*(gridSize + 2)*(gridSize + 2) * sizeof(float));
}


CPUFluidSolver3D::~CPUFluidSolver3D(){

	delete this->u;
	delete this->v;
	delete this->w;
	delete this->density;

	delete this->uPrev;
	delete this->vPrev;
	delete this->wPrev;
	delete this->densityPrev;
}


void CPUFluidSolver3D::Init() {
	//load state
}



void CPUFluidSolver3D::Update(double frameTime) {

	this->frameTime = frameTime;
	VelocityStep(this->u, this->v, this->w, this->uPrev, this->vPrev, this->wPrev, 0.f);
	DensityStep(this->density, this->densityPrev, this->u, this->v, this->w, 0.f);
	KeepSourcesConstant();
}


float * CPUFluidSolver3D::GetVelocityX() {
	return this->u;
}

float * CPUFluidSolver3D::GetVelocityY() {
	return this->v;
}

float * CPUFluidSolver3D::GetDensity() {
	return this->density;
}

float * CPUFluidSolver3D::GetVelocityW() {
	return this->w;
}


void CPUFluidSolver3D::KeepSourcesConstant() {
	
	memset(this->uPrev, 0.f, (this->gridSize + 2) * (this->gridSize + 2) * (this->gridSize + 2) * sizeof(float));
	memset(this->vPrev, 0.f, (this->gridSize + 2) * (this->gridSize + 2) * (this->gridSize + 2) * sizeof(float));
	memset(this->wPrev, 0.f, (this->gridSize + 2) * (this->gridSize + 2) * (this->gridSize + 2) * sizeof(float));
	memset(this->densityPrev, 0.f, (this->gridSize + 2) * (this->gridSize + 2) * (this->gridSize + 2) * sizeof(float));
}


void CPUFluidSolver3D::AddDensity(glm::ivec3 coords, float flow) {

	this->densityPrev[INDEX(coords.x, coords.y, coords.z)] = flow; //good flow 200.f
}


void CPUFluidSolver3D::AddForce(glm::ivec3 coords, float force) {

	this->uPrev[INDEX(coords.x, coords.y, coords.z)] = 0;	//10.f is great force
	this->vPrev[INDEX(coords.x, coords.y, coords.z)] = 0; //10.f is great force
	this->wPrev[INDEX(coords.x, coords.y, coords.z)] = force;
}


bool CPUFluidSolver3D::CAPTURESTATE() {

	unsigned int size = (this->gridSize + 2) * (this->gridSize + 2) * (this->gridSize + 2);

	bool retval = true;

	retval &= Utils::WriteToClearFile("a0.txt", this->u, sizeof(float), size);
	retval &= Utils::WriteToClearFile("a1.txt", this->v, sizeof(float), size);
	retval &= Utils::WriteToClearFile("a2.txt", this->w, sizeof(float), size);
	retval &= Utils::WriteToClearFile("a3.txt", this->density, sizeof(float), size);
	retval &= Utils::WriteToClearFile("a4.txt", this->uPrev, sizeof(float), size);
	retval &= Utils::WriteToClearFile("a5.txt", this->vPrev, sizeof(float), size);
	retval &= Utils::WriteToClearFile("a6.txt", this->wPrev, sizeof(float), size);
	retval &= Utils::WriteToClearFile("a7.txt", this->densityPrev, sizeof(float), size);
	return retval;
}


bool CPUFluidSolver3D::LOADSTATE() {

	bool retval = true;
	void* data_ptr;
	unsigned int lenght;
	retval &= Utils::ReadFromFile("a0.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->u, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("a1.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->v, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("a2.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->w, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("a3.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->density, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("a4.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->uPrev, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("a5.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->vPrev, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("a6.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->wPrev, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("a7.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->densityPrev, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	
	return retval;
}


std::string CPUFluidSolver3D::DEBUGSTRING(float* vec) {
	string s;
	for (int i = 0; i < (this->gridSize + 2)*(this->gridSize + 2)*(this->gridSize + 2); ++i) {
		s += std::to_string(vec[i]) + "|";
	}
	return s;
}


/*PPP*/
void CPUFluidSolver3D::ULTIMATEDEBUG(float * p) {

	int max = ((this->gridSize + 2)*(this->gridSize + 2)*(this->gridSize + 2));
	std::string str_p;
	for (size_t i = 0; i < max; ++i) {
		if (p[i] > 0.0001 || p[i] < -0.0001) {
			str_p += std::to_string(p[i]) + "@" + std::to_string(i) + "|";
		}
	}

	Utils::PrintOut("\n\n" + str_p);
}


void CPUFluidSolver3D::AddSource(float * target, float * source) {

	size_t max = (this->gridSize + 2)*(this->gridSize + 2)*(this->gridSize + 2);

	for (int i = 0; i < max; ++i) {
		target[i] += frameTime * source[i];
	}
}


void CPUFluidSolver3D::Diffuse(BOUNDARY boundarySelection, float * src, float * src_prev, float diffuse) {

	float a = this->frameTime * diffuse* this->gridSize * this->gridSize * this->gridSize;

	//gauss seidl relaxation
	for (int o = 0; o < 10; o++) {
		for (int i = 1; i <= this->gridSize; ++i) {
			for (int j = 1; j <= this->gridSize; ++j) {
				for (int k = 1; k <= this->gridSize; ++k) {
					src[INDEX(i, j, k)] = (src_prev[INDEX(i, j, k)] + a * (src[INDEX(i - 1, j, k)] + src[INDEX(i + 1, j, k)] + src[INDEX(i, j - 1, k)] + src[INDEX(i, j + 1, k)] + src[INDEX(i, j, k - 1)] + src[INDEX(i, j, k + 1)])) / (1 + 6 * a);
				}
			}
		}
		SetBoundaries(boundarySelection, src);
	}
}


void CPUFluidSolver3D::Advect(BOUNDARY boundary, float * density, float * density_prev, float * u, float * v, float * w) {

	int i0, j0, k0, i1, j1, k1;
	float x, y, z, x_frac_negative, y_frac_negative, z_frac_negative, x_frac_positive, y_frac_positive, z_frac_positive;

	float dt0 = this->frameTime * this->gridSize;

	for (int i = 1; i <= this->gridSize; i++) {
		for (int j = 1; j <= this->gridSize; j++) {
			for (int k = 1; k <= this->gridSize; k++) {
				//x vetev
				x = i - dt0 * u[INDEX(i, j, k)];
				if (x < 0.5f) {
					x = 0.5f;
				}
				if (x > this->gridSize + 0.5) {
					x = 0.5 + this->gridSize;
				}
				i0 = (int)x;
				i1 = i0 + 1;
				//y vetev
				y = j - dt0 * v[INDEX(i, j, k)];
				if (y < 0.5f) {
					y = 0.5f;
				}
				if (y > this->gridSize + 0.5) {
					y = 0.5 + this->gridSize;
				}
				j0 = (int)y;
				j1 = j0 + 1;
				//z vetev
				z = k - dt0 * w[INDEX(i, j, k)];
				if (z < 0.5f) {
					z = 0.5f;
				}
				if (z > this->gridSize + 0.5) {
					z = 0.5 + this->gridSize;
				}
				k0 = (int)z;
				k1 = k0 + 1;

				x_frac_positive = x - i0; //dec point of x
				x_frac_negative = 1.0f - x_frac_positive; // 1 - dec point of x
				y_frac_positive = y - j0; //dec point of y	
				y_frac_negative = 1.0f - y_frac_positive; // 1 - dec point of y
				z_frac_positive = z - k0; //dec point of z	
				z_frac_negative = 1.0f - z_frac_positive; // 1 - dec point of z

				density[INDEX(i, j, k)] = x_frac_negative *
					(y_frac_negative * z_frac_negative * density_prev[INDEX(i0, j0, k0)]
					 + y_frac_positive * z_frac_negative * density_prev[INDEX(i0, j1, k0)]
					 + y_frac_negative * z_frac_positive * density_prev[INDEX(i0, j0, k1)]
					 + x_frac_positive * z_frac_positive * density_prev[INDEX(i0, j1, k1)])
					+ x_frac_positive *
					(y_frac_negative * z_frac_negative * density_prev[INDEX(i1, j0, k0)]
					 + y_frac_positive * z_frac_negative * density_prev[INDEX(i1, j1, k0)]
					 + y_frac_negative * z_frac_positive * density_prev[INDEX(i1, j0, k1)]
					 + y_frac_positive * z_frac_positive * density_prev[INDEX(i1, j1, k1)]);
			}
		}
	}
	SetBoundaries(boundary, density);

}


void CPUFluidSolver3D::SetBoundaries(CPUFluidSolver3D::BOUNDARY boundary, float * target) {
	
	for (int i = 1; i <=  this->gridSize; i++) {
		for (int j = 1; j <= this->gridSize; j++) {
			target[INDEX(i, j, 0)] = target[INDEX(i, j, 1)] * (boundary == BOUNDARY::STACKED ? -1 : 1);
			target[INDEX(i, j,this->gridSize + 1)] = target[INDEX(i, j, this->gridSize)] * (boundary == BOUNDARY::STACKED ? -1 : 1);

			target[INDEX(0, i, j)] = target[INDEX(1, i, j)] * (boundary == BOUNDARY::HORIZONTAL ? -1 : 1);
			target[INDEX( this->gridSize + 1, i, j)] = target[INDEX(this->gridSize, i, j)] * (boundary == BOUNDARY::HORIZONTAL ? -1 : 1);

			target[INDEX(i, 0, j)] = target[INDEX(i, 1, j)] * (boundary == BOUNDARY::VERTICAL ? -1 : 1);
			target[INDEX(i, this->gridSize + 1, j)] = target[INDEX(i, this->gridSize, j)] * (boundary == BOUNDARY::VERTICAL ? -1 : 1);
		}
	}


	for (int i = 1; i <= this->gridSize; i++) {

		target[INDEX(i, 0, 0)] = 0.5f *(target[INDEX(i, 1, 0)] + target[INDEX(i, 0, 1)]);
		target[INDEX(i, this->gridSize+ 1, 0)] = 0.5f *(target[INDEX(i, this->gridSize, 0)] + target[INDEX(i, this->gridSize+ 1, 1)]);
		target[INDEX(i, 0, this->gridSize + 1)] = 0.5f *(target[INDEX(i, 0, this->gridSize)] + target[INDEX(i, 1, this->gridSize + 1)]);
		target[INDEX(i, this->gridSize+ 1, this->gridSize + 1)] = 0.5f *(target[INDEX(i, this->gridSize, this->gridSize + 1)] + target[INDEX(i, this->gridSize+ 1, this->gridSize)]);
	
		target[INDEX(0, i, 0)] = 0.5f *(target[INDEX(1, i, 0)] + target[INDEX(0, i, 1)]);
		target[INDEX(this->gridSize + 1, i, 0)] = 0.5f *(target[INDEX(this->gridSize, i, 0)] + target[INDEX(this->gridSize + 1, i, 1)]);
		target[INDEX(0, i, this->gridSize + 1)] = 0.5f *(target[INDEX(0, i, this->gridSize)] + target[INDEX(1, i, this->gridSize + 1)]);
		target[INDEX(this->gridSize + 1, i, this->gridSize + 1)] = 0.5f *(target[INDEX(this->gridSize, i, this->gridSize + 1)] + target[INDEX(this->gridSize + 1, i, this->gridSize)]);
	
		target[INDEX(0, 0, i)] = 0.5f *(target[INDEX(0, 1, i)] + target[INDEX(1, 0, i)]);
		target[INDEX(0, this->gridSize+ 1, i)] = 0.5f *(target[INDEX(0, this->gridSize, i)] + target[INDEX(1, this->gridSize+ 1, i)]);
		target[INDEX(this->gridSize + 1, 0, i)] = 0.5f *(target[INDEX(this->gridSize, 0, i)] + target[INDEX(this->gridSize + 1, 1, i)]);
		target[INDEX(this->gridSize + 1, this->gridSize+ 1, i)] = 0.5f *(target[INDEX(this->gridSize + 1, this->gridSize, i)] + target[INDEX(this->gridSize, this->gridSize+ 1, i)]);
	}


	target[INDEX(0, 0 , 0)] = 0.33f * (target[INDEX(1, 0 , 0)] + target[INDEX(0, 1, 0)] + target[INDEX(0, 0, 1)]);
	target[INDEX(0, this->gridSize + 1, 0)] = 0.33f * (target[INDEX(1, this->gridSize + 1, 0)] + target[INDEX(0, this->gridSize, 0)] + target[INDEX(0, this->gridSize + 1, 1)]);
	
	target[INDEX(this->gridSize + 1, 0, 0)] = 0.33f * (target[INDEX(this->gridSize, 0, 0)] + target[INDEX(this->gridSize + 1, 1, 0)] + target[INDEX(this->gridSize + 1, 0, 1)]);
	target[INDEX(this->gridSize + 1, this->gridSize + 1, 0)] = 0.33f * (target[INDEX(this->gridSize, this->gridSize + 1, 0)] + target[INDEX(this->gridSize + 1, this->gridSize, 0)] + target[INDEX(this->gridSize + 1, this->gridSize + 1, 1)]);

	target[INDEX(0, 0, this->gridSize + 1)] = 0.33f * (target[INDEX(1, 0, this->gridSize + 1)] + target[INDEX(0, 1, this->gridSize + 1)] + target[INDEX(0, 0, this->gridSize)]);
	target[INDEX(0, this->gridSize + 1, this->gridSize + 1)] = 0.33f * (target[INDEX(1, this->gridSize + 1, this->gridSize + 1)] + target[INDEX(0, this->gridSize, this->gridSize + 1)] + target[INDEX(0, this->gridSize + 1, this->gridSize)]);

	target[INDEX(this->gridSize + 1, 0, this->gridSize + 1)] = 0.33f * (target[INDEX(this->gridSize, 0, this->gridSize + 1)] + target[INDEX(this->gridSize + 1, 1, this->gridSize + 1)] + target[INDEX(this->gridSize + 1, 0, this->gridSize)]);
	target[INDEX(this->gridSize + 1, this->gridSize + 1, this->gridSize + 1)] = 0.33f * (target[INDEX(this->gridSize, this->gridSize + 1, this->gridSize + 1)] + target[INDEX(this->gridSize + 1, this->gridSize, this->gridSize + 1)] + target[INDEX(this->gridSize + 1, this->gridSize + 1, this->gridSize)]);
}


void CPUFluidSolver3D::DensityStep(float * density, float * density_prev, float * u, float * v, float * w, float diffusion) {

	AddSource(density, density_prev);
	Diffuse(BOUNDARY::NONE, density_prev, density, diffusion);
	Advect(BOUNDARY::NONE, density, density_prev, u, v, w);
}


void CPUFluidSolver3D::VelocityStep(float * u, float * v, float * w, float * u_prev, float * v_prev, float * w_prev, float viscosity) {

	AddSource(u, u_prev);
	AddSource(v, v_prev);
	AddSource(w, w_prev);

	Diffuse(BOUNDARY::HORIZONTAL, u_prev, u, viscosity);
	Diffuse(BOUNDARY::VERTICAL, v_prev, v, viscosity);
	Diffuse(BOUNDARY::STACKED, w_prev, w, viscosity);

	Project(u_prev, v_prev, w_prev, u, v);

	Advect(BOUNDARY::HORIZONTAL, u, u_prev, u_prev, v_prev, w_prev);
	Advect(BOUNDARY::VERTICAL, v, v_prev, u_prev, v_prev, w_prev);
	Advect(BOUNDARY::STACKED, w, w_prev, u_prev, v_prev, w_prev);

	Project(u, v, w, u_prev, v_prev);
}

/*POTENCIONALNI ZLO V H*/
void CPUFluidSolver3D::Project(float * u, float * v, float * w, float * p, float * div) {

	float h = 1.f / this->gridSize;

	for (int i = 1; i <= this->gridSize; i++) {
		for (int j = 1; j <= this->gridSize; j++) {
			for (int k = 1; k <= this->gridSize; k++) {
				div[INDEX(i, j, k)] = -0.33f * ((u[INDEX(i + 1, j, k)] - u[INDEX(i - 1, j, k)]) * h + (v[INDEX(i, j + 1, k)] - v[INDEX(i, j - 1, k)]) * h + (w[INDEX(i, j, k + 1)] - w[INDEX(i, j, k - 1)]) * h); //derivation form all directions
				//div[INDEX(i, j, k)] = -0.33f * h * (u[INDEX(i + 1, j, k)] - u[INDEX(i - 1, j, k)] + v[INDEX(i, j + 1, k)] - v[INDEX(i, j - 1, k)] + w[INDEX(i, j, k + 1)] - w[INDEX(i, j, k - 1)]); //derivation form all directions
				p[INDEX(i, j, k)] = 0.f;
			}
		}
	}
	SetBoundaries(BOUNDARY::NONE, div);
	SetBoundaries(BOUNDARY::NONE, p);

	for (int o = 0; o < 10; o++) {
		for (int i = 1; i <= this->gridSize; i++) {
			for (int j = 1; j <= this->gridSize; j++) {
				for (int k = 1; k <= this->gridSize; k++) {
					p[INDEX(i, j, k)] = (div[INDEX(i, j, k)] + p[INDEX(i - 1, j, k)] + p[INDEX(i + 1, j, k)] + p[INDEX(i, j - 1, k)] + p[INDEX(i, j + 1, k)] + p[INDEX(i, j, k -1)] + p[INDEX(i, j, k + 1)]) / 6.f;
				}
			}
		}
		SetBoundaries(BOUNDARY::NONE, p);
	}

	for (int i = 1; i <= this->gridSize; i++) {
		for (int j = 1; j <= this->gridSize; j++) {
			for (int k = 1; k <= this->gridSize; k++) {
				u[INDEX(i, j, k)] -= 0.5f * (p[INDEX(i + 1, j, k)] - p[INDEX(i - 1, j, k)]) / h;
				v[INDEX(i, j, k)] -= 0.5f * (p[INDEX(i, j + 1, k)] - p[INDEX(i, j - 1, k)]) / h;
				w[INDEX(i, j, k)] -= 0.5f * (p[INDEX(i, j, k + 1)] - p[INDEX(i, j, k - 1)]) / h;
			}
		}
	}
	SetBoundaries(BOUNDARY::HORIZONTAL, u);
	SetBoundaries(BOUNDARY::VERTICAL, v);
	SetBoundaries(BOUNDARY::STACKED, w);
}
