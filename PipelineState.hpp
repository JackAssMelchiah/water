#ifndef PIPELINE_STATE_HPP
#define PIPELINE_STATE_HPP

#include "glm.hpp"
#include "ObjectBuilderHelper.hpp"

class PipelineState: public ObjectBuilderHelper {
public:

	enum class TOPOLOGY {
		TRIANGLE_FAN,
		LINE,
		POINT,
		TRIANGLE
	};
	enum class BLENDING_FUNC {
		ZERO,
		ONE,
		ONE_MINUS_SRC_ALPHA,
		SRC_COLOR,
		DST_COLOR,
		SRC_ALPHA
	};
	enum class DEPT_TEST_FUNC {
		LESS,
		LQUAL,
		GREATER,
		GQUAL
	};
	enum class RASTERIZATION_OPTION {
		TRUE,
		FALSE
	};
	enum class POLYGON_MODE {
		FILL,
		WIREFRAME
	};
	enum class CULL_MODE {
		BACK_F,
		FRONT_F,
		FRONT_BACK_F
	};
	enum class FRONT_FACE {
		CLOCKWISE,
		CCLOCKWISE
	};

	PipelineState();
	virtual ~PipelineState();
	void SetDefaultState();

	virtual void Build() = 0; //@ObjectBuilderHelper

	void SetDepthTest(bool enabled, DEPT_TEST_FUNC func);
	void SetBlending(bool enabled, BLENDING_FUNC src, BLENDING_FUNC dst);
	void SetTopology(TOPOLOGY topology);
	void SetPolygonMode(POLYGON_MODE polymode);
	void SetCulling(bool enabled, CULL_MODE cullmode);
	void SetFrontFace(FRONT_FACE frontFace);
	void SetRasterize(bool enabled);

protected:
	float lineWidth;
	int multisampling;
	
	TOPOLOGY topology;
	POLYGON_MODE polygonMode;
	FRONT_FACE frontFace;

	struct{
		BLENDING_FUNC blendingSrc;
		BLENDING_FUNC blendingDst;
		bool enabled = false;
	}Blending;
	struct{
		DEPT_TEST_FUNC depthTestFunction;
		bool enabled = false;
	}DepthTest;
	struct{
		CULL_MODE cullMode;
		bool enabled = false;
	}FaceCull;
	struct{
		bool enabled = true;
	}Rasterization;
};

#endif