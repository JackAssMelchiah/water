#include "Time.hpp"


Time::Time() {

	this->state = ClockState::STOPPED;
	this->pausedTime = .0;
}



Time::~Time() {
}


/*Starts timer, only if it is stopped*/
void Time::Start() {
	
	if (this->state != ClockState::STOPPED) {
		return;
	}
	this->pausedTime = .0;
	this->state = ClockState::RUNNING;
	this->startTimePoint = std::chrono::system_clock::now();
}


/*Pauses the timer*/
void Time::Pause() {
	//if running then can be paused, and save the clock

	if (this->state != ClockState::RUNNING) {
		return;
	}
	this->state = ClockState::PAUSED;
		
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();

	this->pausedTime = Time::GetMilis(now, this->startTimePoint);

}


/*Resumes paused timer(only)*/
void Time::Resume() {

	if (this->state != ClockState::PAUSED) {
		return;
	}
	this->state = ClockState::RUNNING;
	//time is saved, now overide starting timepoint
	this->startTimePoint = std::chrono::system_clock::now();


}


/*Stops timer -- this resets it*/
void Time::Stop() {

	if (this->state == ClockState::STOPPED) {
		return;
	}

	this->state = ClockState::STOPPED;
	this->pausedTime = .0;

}


/*Returns measured time in miliseconds*/
double Time::GetTime() {

	if (this->state == ClockState::STOPPED) {
		return .0;
	}
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();

	//if timer is paused, then return only the paused time
	if (this->state == ClockState::PAUSED) {
		return this->pausedTime;
	}
	//else return paused time and current measuring time

	return Time::GetMilis(now, this->startTimePoint) + this->pausedTime;
}



/*Returns actual time in miliseconds from two time points*/
double Time::GetMilis(std::chrono::system_clock::time_point& latter, std::chrono::system_clock::time_point& sooner) {

	std::chrono::duration<double, std::ratio<1, 1000>> miliseconds = latter - sooner;

	return miliseconds.count();
}

