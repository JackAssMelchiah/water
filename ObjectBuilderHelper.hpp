#ifndef OBJECT_BUILDER_HELPER_HPP
#define OBJECT_BUILDER_HELPER_HPP

/*This is an interface, which allows objects to be builed separately
* First part is in constructor, second is in initialization
* Also allows to querry if object is fully initialized
* And allows object to build stuctures within them to be used inmidiatelly
*/

#include <cassert>

class ObjectBuilderHelper {
public:
	virtual void Build() = 0;
	virtual void Initialize();
	void CheckInitialized() const;
	void CheckBuilded() const;
	void CheckNOTInitialized() const;
	void CheckNOTBuilded() const;

protected:
	bool initialized = false;
	bool builded = false;
};





#endif // VULKAN_BUILDER_HELPER_HPP
