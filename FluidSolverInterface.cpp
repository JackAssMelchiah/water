#include "FluidSolverInterface.hpp"

//builder needs to be in separate class to the interface

void FluidSolverInterface::LoadSerialized() {

	this->state = STATE::SHOULD_LOAD;
}


void FluidSolverInterface::SaveSerialize() {

	this->state = STATE::SHOULD_SAVE;
}


/*Changes speed of the simulation by required amount of times*/
void FluidSolverInterface::ChangeSpeedTimes(float times) {

	this->symSpeed = times;
}
