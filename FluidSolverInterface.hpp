#ifndef FLUID_SOLV_INTERF_HPP
#define FLUID_SOLV_INTERF_HPP

//FluidSolverInterfaceBuilder ?
class FluidSolverInterface {
public:

	enum class TYPE{SOLVER_2D, SOLVER_3D};
	
	enum class STATE{SHOULD_LOAD, SHOULD_SAVE, NONE};
	virtual void Init() = 0;
	virtual void Update(double frameTime) = 0;
	void LoadSerialized();
	void SaveSerialize();

	void ChangeSpeedTimes(float);
	
protected:

	virtual bool CAPTURESTATE() = 0;
	virtual bool LOADSTATE() = 0;

	STATE state = STATE::NONE;
	float symSpeed = 1.f;
};



#endif // !FLUID_SOLV_INTERF_HPP
