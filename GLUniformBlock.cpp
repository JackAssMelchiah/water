#include "GLUniformBlock.hpp"

GLUniformBlock::UNIFORM_BLOCK_ID GLUniformBlock::_COUNT_ = 0;

GLUniformBlock::GLUniformBlock() {


	this->id = GLUniformBlock::_COUNT_;
	GLUniformBlock::_COUNT_++;
}

GLUniformBlock::~GLUniformBlock() {
}


GLUniformBlock::UNIFORM_BLOCK_ID GLUniformBlock::GetID() const {

	return this->id;
}

/*Returns specified uniform in range 0, GetUniformCount()*/
InternalRenderFormat::UniformBlockMetadata GLUniformBlock::GetUniform(int i) const {
	
	assert(i <= this->GetUniformCount());
	return this->data[i];
}


/*How much uniform is in this block*/
int GLUniformBlock::GetUniformCount() const {
	
	return this->data.size();
}


/*Sets up connection between data which are gonna be buffered*/
void GLUniformBlock::SetNewDataPointer(void * data_ptr, int byteSize) {

	InternalRenderFormat::UniformBlockMetadata new_data;
	new_data.byteSize = byteSize;
	new_data.dataPtr = data_ptr;

	this->data.push_back(new_data);
}


/*Return size of bound blocks*/
int GLUniformBlock::GetBlockSize() const {

	int size = 0;
	for (auto block : this->data) {
		size += block.byteSize;
	}
	return size;
}
