#ifndef BUFFER_HPP
#define BUFFER_HPP
#include <cstring>
#include "Utils.hpp"

class Buffer {
public:
	Buffer();
	virtual ~Buffer();
	virtual bool Initialize(unsigned int bytesize);
	unsigned int GetFreeMemoryOffset() const;
	void Clear();
	virtual void BufferData(void* data, int offset, int size);
	void* GetBufferMemoryToRead(int offset) const;
	unsigned int GetByteSize() const;

protected:

	unsigned int byteSize;
	unsigned int freeMemoryPrt;
	void* data;
	
	
};




#endif // ! BUFFER_HPP
