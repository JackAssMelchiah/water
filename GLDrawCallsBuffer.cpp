#include "GLDrawCallsBuffer.hpp"


GLDrawCallsBuffer::GLDrawCallsBuffer(){

}

GLDrawCallsBuffer::~GLDrawCallsBuffer(){

}


/*Inits the buffer, alocates resources on GPU*/
bool GLDrawCallsBuffer::Initialize(unsigned int bytesize) {
	
	bool ok = true;
	this->byteSize = bytesize;
	
	glCreateBuffers(1, &this->handle);

	//allocate memory on GPU
	glNamedBufferStorage(this->handle, GetByteSize(), nullptr, this->mappingOptions | GL_DYNAMIC_STORAGE_BIT);

	return ok;
}


/*Binds GL_DRAW_INDIRECT_BUFFER*/
void GLDrawCallsBuffer::UseBufferForRender() const {

	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, this->handle);
}


/*Clears GL_DRAW_INDIRECT_BUFFER binding*/
void GLDrawCallsBuffer::UnuseBufferForRender() const {

	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, 0);
}


/*Will map buffer for writing*/
void GLDrawCallsBuffer::OpenBuffer() {
	
	this->data = glMapNamedBufferRange(this->handle, 0, GetByteSize(), this->mappingOptions);
}


/*Returns beginning of the buffer for gl, cannot be used for normal read*/
void* GLDrawCallsBuffer::GetBufferMemoryToRead() const {

	return this->data;
}


/*Returs size on buffer on GPU*/
int GLDrawCallsBuffer::QuerryGPUBufferSize() {

	UseBufferForRender();
	GLint size = 0;
	glGetBufferParameteriv(GL_DRAW_INDIRECT_BUFFER, GL_BUFFER_SIZE, &size);
	UnuseBufferForRender();
	return size;
}


/*Writes data to buffer, offset is in Metadata units*/
void GLDrawCallsBuffer::BufferData(InternalRenderFormat::DrawCommand* commands, int count, int unit_offset) {

	int size = sizeof(InternalRenderFormat::DrawCommand);

	//Utils::PrintOut(std::to_string(commands->count) + ", " + std::to_string(commands->instanceCount) + ", " + std::to_string(commands->baseInstance) + ", " + std::to_string(commands->first));

	//move end buffer if necessary
	int req_byte_space = unit_offset * size + size * count;
	if (req_byte_space > this->freeMemoryPrt) {
		this->freeMemoryPrt = req_byte_space;
	}

	InternalRenderFormat::DrawCommand* dest_ptr = ((InternalRenderFormat::DrawCommand*)this->data) + unit_offset;
	memcpy(dest_ptr, commands, size * count);
}
