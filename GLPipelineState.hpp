#ifndef GL_PIPELINE_STATE
#define GL_PIPELINE_STATE

#include "GL/glew.h"
#include "PipelineState.hpp"
#include "Utils.hpp"


class GLPipelineState: public PipelineState {
public:
	GLPipelineState();
	~GLPipelineState();
	void Build() override;
	GLenum GetTopologyMode() const;

private:

	void ApplyGLPolygonMode();
	void ApplyGLCullMode();
	void ApplyGLFrontFace();
	void ApplyGLRasterize();
	void ApplyGLBlend();
	void ApplyDepthTest();
};




#endif // !GL_PIPELINE_STATE
