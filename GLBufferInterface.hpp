#ifndef GL_BUFFER_INTERFACE_HPP
#define GL_BUFFER_INTERFACE_HPP

#include "GL/glew.h"

class GLBufferInterface {
public:
	GLBufferInterface::GLBufferInterface() {
		#ifdef _DEBUG
		this->mappingOptions = GL_MAP_WRITE_BIT | GL_MAP_READ_BIT;
		#else
		this->mappingOptions = GL_MAP_WRITE_BIT;
		#endif // _DEBUG
	}
	virtual GLBufferInterface::~GLBufferInterface() {
	}
	GLuint GetBufferHandle() const{
		return this->handle;
	}
	virtual void UseBufferForRender() const{
		
		UnuseBufferForCompute();
		glBindBuffer(GL_ARRAY_BUFFER, this->handle);
	}
	virtual void UnuseBufferForRender() const{
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	virtual void UseBufferForCompute(GLuint bindPoint, int offset, int size) const{
		
		UnuseBufferForRender();
		glBindBufferRange(GL_SHADER_STORAGE_BUFFER, bindPoint, this->handle, offset, size);
	}
	virtual void UnuseBufferForCompute() const {

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	}
	virtual int QuerryGPUBufferSize() const{
		
		UseBufferForRender();
		GLint size = 0;
		glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
		UnuseBufferForRender();
		return size;
	}

protected:

	GLbitfield mappingOptions = 0;
	GLuint handle;
};



#endif // !GL_BUFFER_INTERFACE_HPP
