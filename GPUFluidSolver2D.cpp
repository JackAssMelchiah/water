#include "GPUFluidSolver2D.hpp"


GPUFluidSolver2D::GPUFluidSolver2D(const unsigned int gridSize):CPUFluidSolver2D(gridSize) {

	this->data.x = float(gridSize + 2);
}


GPUFluidSolver2D::~GPUFluidSolver2D() {

}


/*Will setup command invocations, will also check if req number of invocations are available */
void GPUFluidSolver2D::Init() {

	// local inv in shader 32 -- dont go bellow 32 * 32 

	GLint available_work_group;
	int invocation_count = int(this->data.x) / 32;
	assert(invocation_count > 0);
	//local 32 is fine glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &work_grp_cnt[0]);

	//check if can be used this config
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &available_work_group);
	assert(invocation_count < available_work_group);

	this->dispatch = invocation_count;
}


/*Updates water simulation*/
void GPUFluidSolver2D::Update(double frameTime) {

	this->data.w = (float)frameTime;

}


/*Returns density for simulation*/
float * GPUFluidSolver2D::GetPrevDensity() {
	return this->densityPrev;
}


/*Returns velocity in X axis for simulation*/
float * GPUFluidSolver2D::GetPrevVelocityX() {
	return this->uPrev;
}


/*Returns velocity in Y axis for simulation*/
float * GPUFluidSolver2D::GetPrevVelocityY() {
	return this->vPrev;
}


/*Getter for sispatch params*/
unsigned int GPUFluidSolver2D::GetDispatch() {

	return this->dispatch;
}


void GPUFluidSolver2D::SetViscosity(float c) {
	
	this->data.y = c;
}

void GPUFluidSolver2D::SetDensity(float c) {

	this->data.z = c;
}

unsigned int GPUFluidSolver2D::GetGridSize() const {
	return unsigned int(this->data.x);
}

float GPUFluidSolver2D::GetViscosity()const {
	return this->data.y;
}

float GPUFluidSolver2D::GetDensity()const {
	return this->data.z;
}

void GPUFluidSolver2D::SetUniformReferences() {

	SaveReference(&this->data, sizeof(this->data));
}
