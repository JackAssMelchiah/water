#ifndef BUFFERABLE_CPU_FLUID_3D
#define BUFFERABLE_CPU_FLUID_3D

#include "CPUFluidSolver3D.hpp"
#include "GPUBufferableInterface.hpp"
#include "GLPipelineState.hpp" 

class BufferableCPUFluid3D: public GPUBufferableInterface, public CPUFluidSolver3D {
public:
	BufferableCPUFluid3D(const unsigned int dimmension);
	virtual ~BufferableCPUFluid3D();

	bool Initialize(GLBuffer* ssbo, GLBuffer* genericBuffer, const unsigned int, const unsigned int);
	void Update(double frameTime);
	GLPipelineState* GetPipelineStateSettings();
	void GetAllVertexFormats(std::vector<GLRenderFormat::VertexFormat>& formats);
	unsigned int GetReqMemorySSBO();
	unsigned int GetReqMemoryGenericBuffer();
	void GetShaders(std::vector<GLShader*>& shaders);
	void GetIncompleteDrawCommand(InternalRenderFormat::IncopleteCommand& comand) override;
protected:
	void CreatePipelineState();
	bool CreateShaders();

private:
	void SetUniformReferences() override;

	GLPipelineState pipelineState;

	GLBuffer* boundVBO;
	unsigned int boundVBOOffset;

	GLBuffer* boundSSBO;
	unsigned int boundSSBOOffset;

	GLShader* boundShader;

	glm::ivec4 dimension;
};


#endif