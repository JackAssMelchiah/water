#include "GLBuffer.hpp"
#define DEBUG

GLBuffer::GLBuffer() {}


GLBuffer::~GLBuffer() {

	if (this->data != nullptr) {
		CloseBuffer();
	}
	glDeleteBuffers(1, &this->handle);
}


/*Initializes buffer size on GPU*/
bool GLBuffer::Initialize(unsigned int byteSize) {
	
	bool ok = true;
	this->byteSize = byteSize;

	glCreateBuffers(1, &this->handle);
	
	//allocate memory on GPU
	glNamedBufferStorage(this->handle, GetByteSize(), nullptr, this->mappingOptions | GL_DYNAMIC_STORAGE_BIT);

	return ok;
}


/*Will map buffer for writing*/
void GLBuffer::OpenBuffer() {
	
	this->data = glMapNamedBufferRange(this->handle, 0, GetByteSize(), this->mappingOptions);
}


/*Will unmap buffer so it can be used for rendering*/
void GLBuffer::CloseBuffer() {

	glUnmapNamedBuffer(this->handle);
	this->data = nullptr;
}


/*In debug mode and buffer must be open, this provides access to buffers' content saved on gpu, with which is returned in retval*/
void GLBuffer::VerifyDataInBuffer(std::vector<float>* retval, int offset, int size) {
	
	#ifndef _DEBUG
	Utils::PrintOutWar("Using helper method VerifyDataInBuffer in release code");
	return;
	#else
	retval->clear();
	retval->resize(size);
	char* src = ((char*)this->data) + offset;
	memcpy(retval->data(), src, size);

	#endif // !_DEBUG
}



