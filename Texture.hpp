#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <string>
#include <SDL_image.h>
#include "GL/glew.h"
#include "glm.hpp"

class Texture{

public:
	typedef int IDTEXTURE;
	Texture(std::string path);
	virtual ~Texture();

	bool LoadTexture();
	void NewTexture(glm::ivec2 dimmensions);

	void DrawImage(GLuint);
	void DrawDiscreteImage(GLuint);
	void SetTextureVariable(std::string var);

	std::string GetTextureName();
	GLuint GetTextureID();
	IDTEXTURE GetID();
	
	glm::ivec2 GetDimensions();

private:
	static int _TEXTURE_COUNT_;
	IDTEXTURE id;
	GLuint textureId;
	


	std::string textureName;
	std::string varName;
	
	glm::ivec2 dimmensions;
};



#endif //  TEXTURE_HPP
