#ifndef GL_BUFFER_HPP
#define GL_BUFFER_HPP

#include "Buffer.hpp"
#include "GLBufferInterface.hpp"
#include "Utils.hpp"


class GLBuffer: public Buffer, public GLBufferInterface {
public:
	GLBuffer();
	virtual ~GLBuffer();
	virtual bool Initialize(unsigned int byteSize);
	virtual void OpenBuffer();
	virtual void CloseBuffer();
	void VerifyDataInBuffer(std::vector<float>* retval, int offset, int size);

};






#endif // !GL_BUFFER_HPP
