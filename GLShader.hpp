#ifndef GL_SHADER_HPP
#define GL_SHADER_HPP

#include "Shader.hpp"
#include "GL/glew.h"

class GLShader: public Shader {
public:
	GLShader();
	~GLShader();

	GLuint GetProgram() const;
	
	bool LoadShader(string computeShaderPath) override;
	bool LoadShader(string vertexShaderPath, string fragmentShaderPath) override;
	bool LoadShader(string vertexShaderPath, string geometryShaderPath, string fragmentShaderPath) override;
	bool LoadShader(string vertexShaderPath, string tessalationControllShaderPath, string tessalationEvaluationShaderPath, string fragmentShaderPath) override;
	bool LoadShader(string vertexShaderPath, string tessalationControllShaderPath, string tessalationEvaluationShaderPath, string geometryShaderPath, string fragmentShaderPath) override;
	void UseShader() const;


	static void PrintfShaderLog(GLuint shader);
	static void PrintProgramLog(GLuint program);

private:
	GLuint LoadShaders(string path, ShaderType type, std::vector<char>* code);
	GLuint programID;
};
#endif // !GL_SHADER_HPP
