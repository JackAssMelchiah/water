#include "ObjectBuilderHelper.hpp"

/*Marks object as initialized*/
void ObjectBuilderHelper::Initialize() {

	this->initialized = true;
}


/*In debug mode asserts initialization*/
void ObjectBuilderHelper::CheckInitialized() const {

	#ifdef _DEBUG
	assert(this->initialized);
	#endif // DEBUG
}


/*In debug mode asserts build*/
void ObjectBuilderHelper::CheckBuilded() const {

	#ifdef _DEBUG
	assert(this->builded);
	#endif // DEBUG
}


/*In debug mode asserts NOT build*/
void ObjectBuilderHelper::CheckNOTBuilded() const {

	#ifdef _DEBUG
	assert(!this->builded);
	#endif // DEBUG
}


/*In debug mode asserts not initialization*/
void ObjectBuilderHelper::CheckNOTInitialized() const {

	#ifdef _DEBUG
	assert(this->initialized);
	#endif // DEBUG
}