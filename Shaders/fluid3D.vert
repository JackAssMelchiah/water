#version 450

layout(location = 0) in vec3 vertices;

layout(std140, binding = 0) uniform matrices{
    mat4 viewMatrix;
    mat4 projectionMatrix;
    ivec4 dimm;
};

layout(std430, binding = 1) buffer DENS{
    float density[];
};

layout(location = 0) out vec4 fragColor;


//https://softwareengineering.stackexchange.com/questions/212808/treating-a-1d-data-structure-as-2d-grid
void main(){

    ivec3 coords;
 
    coords.x = gl_InstanceID % dimm.x;
    coords.y = (gl_InstanceID / dimm.x) % dimm.y;
    coords.z = gl_InstanceID / (dimm.x * dimm.y);


    //translate vertices by some amount, so it gives 1
    vec4 displacement = vec4(coords.x, coords.y, coords.z, 1.f);
    mat4 translation = mat4(1.f);
    translation[3] = displacement;

    //scale vertices by some amount, so it gives 1f
    float displacement_factor = 1.f /(dimm.x);
    mat4 scale = mat4(displacement_factor);
    scale[3][3] = 1.f;

    vec4 final_pos = scale * translation * vec4(vertices, 1.f);
    gl_Position = projectionMatrix * viewMatrix * final_pos;


    //color index
    int col_i = gl_InstanceID;
    fragColor = vec4(vec3(density[col_i] * 2), 0.05f);
}