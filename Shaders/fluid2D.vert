#version 450
layout(location = 0) in vec3 position;

layout(std140, binding = 0) uniform matrices{
    mat4 viewMatrix;
    mat4 projectionMatrix;
};

layout(std430, binding = 1) buffer DENS{
    float density[];
};

layout(location = 0) out vec3 fragColor;

void main(){

    int index = gl_VertexID / 6;

    vec3 final_pos = vec3(position.x, position.y - density[index], position.z);
    gl_Position = projectionMatrix * viewMatrix * vec4(final_pos, 1.0);
    fragColor = vec3(0.1, 2 *density[index], 0.3);
}