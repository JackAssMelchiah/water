#include "CPUFluidSolver2D.hpp"

#define INDEX(i, j) ((i) + (this->gridSize + 2) * (j)) 


CPUFluidSolver2D::CPUFluidSolver2D(const unsigned int gridSize) :gridSize(gridSize) {

	//allocation
	this->uPrev = new float[(gridSize + 2)*(gridSize + 2)];
	this->vPrev = new float[(gridSize + 2)*(gridSize + 2)];
	this->densityPrev = new float[(gridSize + 2)*(gridSize + 2)];

	memset(this->uPrev, 0, (gridSize + 2)*(gridSize + 2) * sizeof(float));
	memset(this->vPrev, 0, (gridSize + 2)*(gridSize + 2) * sizeof(float));
	memset(this->densityPrev, 0, (gridSize + 2)*(gridSize + 2) * sizeof(float));
	//for now
	this->density = new float[(gridSize + 2)*(gridSize + 2)];
	memset(this->density, 0, (gridSize + 2)*(gridSize + 2) * sizeof(float));

	this->u = new float[(gridSize + 2)*(gridSize + 2)];
	this->v = new float[(gridSize + 2)*(gridSize + 2)];

	memset(this->u, 0, (gridSize + 2)*(gridSize + 2) * sizeof(float));
	memset(this->v, 0, (gridSize + 2)*(gridSize + 2) * sizeof(float));
}


void CPUFluidSolver2D::Init() {

	//LOADSTATE();
}


CPUFluidSolver2D::~CPUFluidSolver2D() {

	delete this->u;
	delete this->v;
	delete this->density;

	delete this->uPrev;
	delete this->vPrev;
	delete this->densityPrev;
}


/*Clears all prev entries, so that simulation wont blow up*/
void CPUFluidSolver2D::KeepSourcesConstant() {

	memset(this->uPrev, 0.f, (this->gridSize + 2) * (this->gridSize + 2) * sizeof(float));
	memset(this->vPrev, 0.f, (this->gridSize + 2) * (this->gridSize + 2) * sizeof(float));
	memset(this->densityPrev, 0.f, (this->gridSize + 2) * (this->gridSize + 2) * sizeof(float));
}


/*Simulation step, with actualized frame time, expects applied forces and density with AddDensity and AddForce*/
void CPUFluidSolver2D::Update(double frameTime) {
	
	this->frameTime = frameTime;
	/*if (this->record) {
		CAPTURESTATE();
		this->record = false;
	}*/
	VelocityStep(this->u, this->v, this->uPrev, this->vPrev, 0.f);
	DensityStep(this->density, this->densityPrev, this->u, this->v, 0.f);
	KeepSourcesConstant();
}


/*Returns current velocities in x coords*/
float* CPUFluidSolver2D::GetVelocityX() {
	
	return this->u;
}


/*Returns current velocities in y coords*/
float* CPUFluidSolver2D::GetVelocityY() {

	return this->v;
}


/*Returns density, nonexpanded*/
float* CPUFluidSolver2D::GetDensity() {

	return this->density;
}


/*Adds density to current density array at coords*/
void CPUFluidSolver2D::AddDensity(glm::ivec2 coords, float flow) {

	this->densityPrev[INDEX(coords.x, coords.y)] = flow; //good flow 200.f
}


/*Adds force into the system, on which base will the density move*/
void CPUFluidSolver2D::AddForce(glm::ivec2 coords, float force) {

	this->uPrev[INDEX(coords.x, coords.y)] = force;	//10.f is great force
	this->vPrev[INDEX(coords.x, coords.y)] = 0; //10.f is great force
}


/*Adds fluid source to the grid*/
void CPUFluidSolver2D::AddSource(float* target, float* source) {
	
	size_t max = (this->gridSize + 2)*(this->gridSize + 2);

	for (int i = 0; i < max; ++i) {
		target[i] += frameTime * source[i]; 
	}
}


/*Diffuses the matter within the array, based on pre_array and diffuse constant - uses stable backward diffusion*/
void CPUFluidSolver2D::Diffuse(BOUNDARY boundarySelection, float* src, float* src_prev, float diffuse) {

	float a = this->frameTime * diffuse* this->gridSize * this->gridSize;

	//gauss seidl relaxation
	for (int k = 0; k < 10; k++) {
		for (int i = 1; i <= this->gridSize; ++i) {
			for (int j = 1; j <= this->gridSize; ++j) {
				src[INDEX(i, j)] = (src_prev[INDEX(i, j)] + a * (src[INDEX(i-1, j)] + src[INDEX(i+1, j)] + src[INDEX(i, j-1)] + src[INDEX(i, j+1)])) / (1 + 4 * a);
			}
		}
		SetBoundaries(boundarySelection, src);
	}
}


/*Forces density to folow velocity field*/
void CPUFluidSolver2D::Advect(BOUNDARY boundary, float* density, float* density_prev, float* u, float* v) {

	int i0, j0, i1, j1;
	float x, y, x_frac_negative, y_frac_negative, x_frac_positive, y_frac_positive;

	float dt0 = this->frameTime * this->gridSize;

	for (int i = 1; i <= this->gridSize; i++) {
		for (int j = 1; j <= this->gridSize; j++) {
			//x vetev
			x = i - dt0 * u[INDEX(i, j)];	
			if (x < 0.5f) {
				x = 0.5f;
			}
			if (x > this->gridSize + 0.5) {
				x = 0.5 + this->gridSize;
			}
			i0 = (int)x;
			i1 = i0 + 1;
			//y vetev
			y = j - dt0 * v[INDEX(i, j)];
			if (y < 0.5f) {
				y = 0.5f;
			}
			if (y > this->gridSize + 0.5) {
				y = 0.5 + this->gridSize;
			}
			j0 = (int)y;
			j1 = j0 + 1;
			//
			x_frac_positive = x - i0; //dec point of x
			x_frac_negative = 1.0f - x_frac_positive; // 1 - dec point of x
			y_frac_positive = y - j0; //dec point of y	
			y_frac_negative = 1.0f - y_frac_positive; // 1 - dec point of y

			density[INDEX(i, j)] = x_frac_negative * (y_frac_negative * density_prev[INDEX(i0, j0)] + y_frac_positive * density_prev[INDEX(i0, j1)]) + x_frac_positive * (y_frac_negative * density_prev[INDEX(i1, j0)] + y_frac_positive * density_prev[INDEX(i1, j1)]);
		}
	}
	SetBoundaries(boundary, density);
}


/*Density solver step*/
void CPUFluidSolver2D::DensityStep(float* density, float* density_prev, float* u, float* v, float diffusion) {

	AddSource(density, density_prev);
	Diffuse(BOUNDARY::NONE, density_prev, density, diffusion); //swaped densities
	Advect(BOUNDARY::NONE, density, density_prev, u, v);
}


/*Velocity solver step*/
void CPUFluidSolver2D::VelocityStep(float* u, float* v, float* u_prev, float* v_prev, float viscosity) {

	AddSource(u, u_prev);
	AddSource(v, v_prev);

	Diffuse(BOUNDARY::HORIZONTAL, u_prev, u, viscosity); //swapped horizontal velocities
	Diffuse(BOUNDARY::VERTICAL, v_prev, v, viscosity);	 //swapped vertical velocities
	Project(u_prev, v_prev, u, v); //still swapped
	Advect(BOUNDARY::HORIZONTAL, u, u_prev, u_prev, v_prev);
	Advect(BOUNDARY::VERTICAL, v, v_prev, u_prev, v_prev);
	Project(u, v, u_prev, v_prev);
}


/*Mass conservation routine for velocity - uses Poisson equation*/
void CPUFluidSolver2D::Project(float* u, float* v, float* p, float* div) {

	float h = 1.f / this->gridSize;

	for (int i = 1; i <= this->gridSize; i++) {
		for (int j = 1; j <= this->gridSize; j++) {
			div[INDEX(i, j)] = -0.5f * h * (u[INDEX(i+1, j)] - u[INDEX(i-1, j)] + v[INDEX(i, j+1)] - v[INDEX(i, j-1)]); //derivation form both directions
			p[INDEX(i, j)] = 0.f;
		}
	}
	SetBoundaries(BOUNDARY::NONE, div);
	SetBoundaries(BOUNDARY::NONE, p);

	//gauss seidl relaxation from difusse
	for (int k = 0; k < 10; k++) {
		for (int i = 1; i <= this->gridSize; i++) {
			for (int j = 1; j <= this->gridSize; j++) {
				p[INDEX(i, j)] = (div[INDEX(i, j)] + p[INDEX(i-1, j)] + p[INDEX(i+1, j)] + p[INDEX(i, j-1)] + p[INDEX(i, j+1)]) / 4.f;
			}
		}
		SetBoundaries(BOUNDARY::NONE, p);
	}

	for (int i = 1; i <= this->gridSize; i++) {
		for (int j = 1; j <= this->gridSize; j++) {
			u[INDEX(i, j)] -= 0.5f * (p[INDEX(i+1, j)] - p[INDEX(i-1, j)]) / h;
			v[INDEX(i, j)] -= 0.5f * (p[INDEX(i, j+1)] - p[INDEX(i, j-1)]) / h;
		}
	}
	SetBoundaries(BOUNDARY::HORIZONTAL, u);
	SetBoundaries(BOUNDARY::VERTICAL, v);
}


/*Corrects the mass in target to not go out of bounds -- its assumed to be within bounding box*/
void CPUFluidSolver2D::SetBoundaries(CPUFluidSolver2D::BOUNDARY boundary, float* target) {

	for (int i = 1; i <= this->gridSize; i++) {
		target[INDEX(0, i)]						= target[INDEX(1, i)] * (boundary == BOUNDARY::HORIZONTAL ? -1 : 1);
		target[INDEX(this->gridSize+1, i)]		= target[INDEX(this->gridSize, i)] * (boundary == BOUNDARY::HORIZONTAL ? -1 : 1);

		target[INDEX(i, 0)]						= target[INDEX(i, 1)] * (boundary == BOUNDARY::VERTICAL ? -1 : 1);
		target[INDEX(i, this->gridSize + 1)]	= target[INDEX(i, this->gridSize)] * (boundary == BOUNDARY::VERTICAL ? -1 : 1);
	}

	target[INDEX(0, 0)] = 0.5f * (target[INDEX(1, 0)] + target[INDEX(0, 1)]);
	target[INDEX(0, this->gridSize + 1)] = 0.5f * (target[INDEX(1, this->gridSize + 1)] + target[INDEX(0, this->gridSize)]);
	target[INDEX(this->gridSize + 1, 0)] = 0.5f * (target[INDEX(this->gridSize, 0)] + target[INDEX(this->gridSize + 1, 1)]);
	target[INDEX(this->gridSize + 1, this->gridSize + 1)] = 0.5f * (target[INDEX(this->gridSize + 1, this->gridSize)] + target[INDEX(this->gridSize, this->gridSize + 1)]);
}


std::string CPUFluidSolver2D::DEBUGSTRING(float* vec) {
	string s;
	for (int i = 0; i < (this->gridSize + 2)*( this->gridSize + 2); ++i) {
			s += std::to_string(vec[i]) + "|";
	}
	return s;
}


/*PPP*/
void CPUFluidSolver2D::ULTIMATEDEBUG(float * p) {

	int max = ((this->gridSize + 2)*(this->gridSize + 2));
	std::string str_p;
	for (size_t i = 0; i < max; ++i) {
		if (p[i] > 0.0001 || p[i] < -0.0001) {
			str_p += std::to_string(p[i]) + "@" + std::to_string(i) + "|";
		}
	}

	Utils::PrintOut("\n\n" + str_p);
}


/*Captures state of all buffers, saves them to disk*/
bool CPUFluidSolver2D::CAPTURESTATE() {

	unsigned int size = (this->gridSize+2) * (this->gridSize+2);
	bool retval = true;

	retval &= Utils::WriteToClearFile("b0.txt", this->u, sizeof(float), size);
	retval &= Utils::WriteToClearFile("b1.txt", this->v, sizeof(float), size);
	retval &= Utils::WriteToClearFile("b2.txt", this->density, sizeof(float), size);
	retval &= Utils::WriteToClearFile("b3.txt", this->uPrev, sizeof(float), size);
	retval &= Utils::WriteToClearFile("b4.txt", this->vPrev, sizeof(float), size);
	retval &= Utils::WriteToClearFile("b5.txt", this->densityPrev, sizeof(float), size);

	return retval;
}


/*Captures state of all buffers, saves them to disk*/
bool CPUFluidSolver2D::LOADSTATE() {

	void* data_ptr;
	unsigned int lenght;
	bool retval = true;
	retval &= Utils::ReadFromFile("b0.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->u, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("b1.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->v, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("b2.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->density, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("b3.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->uPrev, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("b4.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->vPrev, data_ptr, lenght * sizeof(float));
	delete data_ptr;
	retval &= Utils::ReadFromFile("b5.txt", sizeof(float), &data_ptr, lenght);
	memcpy(this->densityPrev, data_ptr, lenght * sizeof(float));
	delete data_ptr;

	return retval;
}

