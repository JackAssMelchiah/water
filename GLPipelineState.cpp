#include "GLPipelineState.hpp"


GLPipelineState::GLPipelineState():PipelineState() {
	
}


GLPipelineState::~GLPipelineState() {

}


/*Applies the state to GL state machine, set it to default state first*/
void GLPipelineState::Build() {

	CheckInitialized();
	
	//convert all states to glStates
	ApplyGLCullMode();
	ApplyGLFrontFace();
	ApplyGLPolygonMode();
	ApplyGLRasterize();
	ApplyDepthTest();
	ApplyGLBlend();
	
	this->builded = true;
}



/*Returns drawn polygon mode -- for renderer*/
GLenum GLPipelineState::GetTopologyMode() const{

	switch (this->topology) {
		case TOPOLOGY::LINE:{
			return GL_LINE;
			break;
		}
		case TOPOLOGY::POINT:{
			return GL_POINTS;
			break;
		}
		case TOPOLOGY::TRIANGLE:{
			return GL_TRIANGLES;
			break;
		}
		case TOPOLOGY::TRIANGLE_FAN:{
			return GL_TRIANGLE_FAN;
			break;
		}
		default:{
			Utils::PrintOutWar("TOPOLOGY TRANSLATION ERROR");
			return GL_TRIANGLES;
			break;
		}
	}
}


/*Apllies all setings related to depth test*/
void GLPipelineState::ApplyDepthTest() {

	GLenum func_result;
	
	if (this->DepthTest.enabled) {
		glEnable(GL_DEPTH_TEST);
	}
	else {
		glDisable(GL_DEPTH_TEST);
	}

	switch (this->DepthTest.depthTestFunction){
	case DEPT_TEST_FUNC::LESS: {
		func_result = GL_LESS;
		break;
	}
	case DEPT_TEST_FUNC::GREATER: {
		func_result = GL_GREATER;
		break;
	}
	case DEPT_TEST_FUNC::GQUAL: {
		func_result = GL_GEQUAL;
		break;
	}
	case DEPT_TEST_FUNC::LQUAL: {
		func_result = GL_LEQUAL;
		break;
	}
	default:
		func_result = GL_LESS;
		break;
	}
	glDepthFunc(func_result);
}


/*Applies polygon mode settings*/
void GLPipelineState::ApplyGLPolygonMode() {

	if (this->polygonMode == POLYGON_MODE::FILL) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
}


/*Applies culling mode settings*/
void GLPipelineState::ApplyGLCullMode() {

	if (this->FaceCull.enabled) {
		glEnable(GL_CULL_FACE);
	}
	else {
		glDisable(GL_CULL_FACE);
	}
	  
	switch (this->FaceCull.cullMode) {
		case CULL_MODE::BACK_F: {
			glCullFace(GL_BACK);
			break;
		}
		case CULL_MODE::FRONT_F: {
			glCullFace(GL_FRONT);
			break;
		}
		case CULL_MODE::FRONT_BACK_F: {
			glCullFace(GL_FRONT_AND_BACK);
			break;
		}
		default:{
			Utils::PrintOutWar("CULL MODE ERROR");
			break;
		}
	}
}


/*Applies FrontFace settings*/
void GLPipelineState::ApplyGLFrontFace() {

	switch (this->frontFace) {
		case FRONT_FACE::CLOCKWISE: {
			glFrontFace(GL_CW);
			break;
		}
		case FRONT_FACE::CCLOCKWISE: {
			glFrontFace(GL_CCW);
			break;
		}
		default:{
			Utils::PrintOutWar("FRONT FACE ERROR");
			break;
		}
	}
}


/*Applies Rasterization settings*/
void GLPipelineState::ApplyGLRasterize() {

	if (this->Rasterization.enabled) {
		glDisable(GL_RASTERIZER_DISCARD);
	}
	else {
		glEnable(GL_RASTERIZER_DISCARD);
	}
}


/*Applies blending*/
void GLPipelineState::ApplyGLBlend() {

	if(!this->Blending.enabled){
		glDisable(GL_BLEND);
		return;
	}

	GLenum src_blend;
	glEnable(GL_BLEND);

	switch (this->Blending.blendingSrc) {
		case BLENDING_FUNC::DST_COLOR:{
			src_blend = GL_DST_COLOR;
			break;
		}
		case BLENDING_FUNC::ONE:{
			src_blend = GL_ONE;
			break;
		}
		case BLENDING_FUNC::ZERO:{
			src_blend = GL_ZERO;
			break;
		}
		case BLENDING_FUNC::ONE_MINUS_SRC_ALPHA:{
			src_blend = GL_ONE_MINUS_SRC_ALPHA;
			break;
		}
		case BLENDING_FUNC::SRC_COLOR:{
			src_blend = GL_SRC_COLOR;
			break;
		}
		case BLENDING_FUNC::SRC_ALPHA:{
			src_blend = GL_SRC_ALPHA;
			break;
		}
		default:{
			Utils::PrintOutWar("SRC BLEND ERROR");
			break;
		}
	}

	switch (this->Blending.blendingDst) {
		case BLENDING_FUNC::DST_COLOR:{
			glBlendFunc(src_blend, GL_DST_COLOR);
			break;
		}
		case BLENDING_FUNC::ONE:{
			glBlendFunc(src_blend, GL_ONE);
			break;
		}
		case BLENDING_FUNC::ZERO:{
			glBlendFunc(src_blend, GL_ZERO);
			break;
		}
		case BLENDING_FUNC::ONE_MINUS_SRC_ALPHA:{
			glBlendFunc(src_blend, GL_ONE_MINUS_SRC_ALPHA);
			break;
		}
		case BLENDING_FUNC::SRC_COLOR:{
			glBlendFunc(src_blend, GL_SRC_COLOR);
			break;
		}
		case BLENDING_FUNC::SRC_ALPHA:{
			glBlendFunc(src_blend, GL_SRC_ALPHA);
			break;
		}
		default:{
			Utils::PrintOutWar("DST BLEND ERROR");
			break;
		}
	}
}