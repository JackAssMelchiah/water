#ifndef WATER_RENDERER_HPP
#define WATER_RENDERER_HPP

#include <string>
#include "SDL.h"
#include "glm.hpp"
#include <cmath>
#include <string.h>

#include "GLShader.hpp"
#include "GLBuffer.hpp"
#include "GLUniformBuffer.hpp"
#include "GLDrawCallsBuffer.hpp"

#include "GLRenderFormat.hpp"
#include "Shape.hpp"
#include "Time.hpp"
#include "InputManager.hpp"
#include "FpsCamera.hpp"
#include "FluidSolverFactory.hpp"


class WaterRenderer {
public:
	WaterRenderer(SDL_Window* w, const unsigned int);
	~WaterRenderer();
	bool Init(FluidSolverFactory::TYPE type);
	void Loop();

private:
	enum class MAIN_INPUT_TYPE {NONE, QUIT};
	void AddUniform(UniformAttributeInterfaceObject* object, GLUniformBlock & targetBlock);
	void AddUniform(UniformAttributeInterfaceObject& object, GLUniformBlock& targetBlock);
	void ApplyVertexFormat(GLRenderFormat::VertexFormat& format);
	void Update(double frameTime);
	//void Compute();
	void Render();
	void Clear();
	void Swap();
	MAIN_INPUT_TYPE HandleInput(std::vector<SDL_Event>&, double);
	void HandleTime(double &frameTime);
	void EnableDebug();
	static void DefaultDebugMessage(GLenum source, GLenum type, GLuint, GLenum severity, GLsizei, const GLchar * message, void *);
	static std::string TranslateDebugSource(GLenum source);
	static std::string TranslateDebugType(GLenum type);
	static std::string TranslateDebugSeverity(GLenum severity);

	void DebugGPUOutputs();
	void DebugCPUOutputs();

	SDL_Window* window = nullptr;
	GLBuffer dataBuffer;
	GLBuffer ssbo;
	GLUniformBuffer uniformBuffer;
	GLUniformBlock uniformBlock;
	GLDrawCallsBuffer drawCallsBuffer;
	InputManager im;
	FpsCamera camera;
	Time timer;
	GLShader *shader;
	GLShader computeShader;
	GLuint VAO;

	const unsigned int waterRows;
	
	GPUBufferableInterface *water = nullptr;

	FluidSolverFactory::TYPE current_type;
};




#endif // ! WATER_RENDERER_HPP
