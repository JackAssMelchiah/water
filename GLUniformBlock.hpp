#ifndef _GL_UNIFORM_BLOCK_HPP_
#define _GL_UNIFORM_BLOCK_HPP_

#include "InternalRenderFormat.hpp"
#include <vector>

class GLUniformBlock {
public:
	typedef int UNIFORM_BLOCK_ID;
	GLUniformBlock();
	~GLUniformBlock();
	UNIFORM_BLOCK_ID GetID() const;
	InternalRenderFormat::UniformBlockMetadata GetUniform(int) const;
	int GetUniformCount() const;
	void SetNewDataPointer(void* data_ptr, int byteSize);
	int GetBlockSize() const;
private:

	std::vector<InternalRenderFormat::UniformBlockMetadata> data;
	UNIFORM_BLOCK_ID id;
	static UNIFORM_BLOCK_ID _COUNT_;
};



#endif // !_GL_UNIFORM_BLOCK_HPP_
