#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "SDL.h"
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtx/rotate_vector.hpp"
#include "UniformAttributeInterfaceObject.hpp"

class Camera: public UniformAttributeInterfaceObject {

public:
	Camera(SDL_Window *, glm::vec3 cameraPosition, glm::vec3 cameraDirection, float FOV, float aspectRatio, glm::vec2 nearFarPlane);
	Camera(SDL_Window *);
	virtual ~Camera();

	void ChangeFov(float fov);
	void ChangeAspectRatio(float ratio);
	void ChangeNearFarPlanes(glm::vec2 planes);
	glm::vec3* GetPostion();
	glm::vec3* GetDirection();
	float GetFoV();
	SDL_Window* GetWindow();
	virtual void UpdateCamera(); //for now


	glm::mat4* GetProjection();
	glm::mat4* GetView();

	void SetUniformReferences() override;

protected:

	void ChangeViewDirection(glm::vec3 dir);
	void ChangePosition(glm::vec3 pos);

private:
	void RecalcProjectionMatrix();

	glm::mat4 viewMatrix;
	glm::vec3 cameraPosition;
	glm::vec3 lookDirection;

	glm::mat4 projectionMatrix;
	glm::vec2 nearFarPlane;
	float aspectRatio;
	float fov;
	
	SDL_Window *window;

};

#endif // !CAMERA_HPP