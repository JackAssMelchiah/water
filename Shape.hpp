#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "glm.hpp"
#include "Utils.hpp"


class Shape {
public:
	static void Axies(std::vector<glm::vec3>&vertices, std::vector<glm::vec3>&colors);
	static void Cube(std::vector<glm::vec3>&vertices, std::vector<glm::vec3>&colors);
	static void Grid(std::vector<glm::vec3>& vertices, unsigned int rowPointsCount);
private:
	static void ParametricCube(std::vector<glm::vec3>&vertices, float width, float height, float depth, glm::vec3 unitOrigin);
};

#endif // !SHAPE_HPP
