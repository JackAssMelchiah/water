#include "GLIndexBuffer.hpp"
#define DEBUG

GLIndexBuffer::GLIndexBuffer() {}


GLIndexBuffer::~GLIndexBuffer() {

	CloseBuffer();
	glDeleteBuffers(1, &this->handle);
}


void GLIndexBuffer::UseBufferForRender() const {

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->handle);
}


void GLIndexBuffer::UnuseBufferForRender() const {

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


int GLIndexBuffer::QuerryGPUBufferSize() const {

	UseBufferForRender();
	GLint size = 0;
	glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
	UnuseBufferForRender();
	return size;
}

