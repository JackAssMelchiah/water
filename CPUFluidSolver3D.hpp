#ifndef CPU_FLUID_SOLVER_3D_HPP
#define CPU_FLUID_SOLVER_3D_HPP

#include "FluidSolverInterface.hpp"
#include <string>
#include "glm.hpp"
#include "Utils.hpp"


class CPUFluidSolver3D: public FluidSolverInterface{

public:
	CPUFluidSolver3D(const unsigned int gridSize);
	virtual ~CPUFluidSolver3D();

	void Init() override;
	void Update(double frameTime) override;
	bool CAPTURESTATE() override;
	bool LOADSTATE() override;

	float * GetVelocityX();
	float * GetVelocityY();
	float * GetVelocityW();
	float * GetDensity();
	void AddDensity(glm::ivec3 coords, float flow);
	void AddForce(glm::ivec3 coords, float force);
	void KeepSourcesConstant();

	void ULTIMATEDEBUG(float * p);
	std::string DEBUGSTRING(float* vec);

protected:

	float* u;
	float* v;
	float* w;
	float* density;
	float* vPrev;
	float* uPrev;
	float* wPrev;
	float* densityPrev;
	double frameTime = 0.01667;
	const unsigned int gridSize;

private:
	enum class BOUNDARY { NONE, HORIZONTAL, VERTICAL, STACKED};

	void AddSource(float* target, float* source);
	void Diffuse(BOUNDARY boundarySelection, float* src, float* src_prev, float diffuse);
	void Advect(BOUNDARY boundary, float* density, float* density_prev, float* u, float* v, float* w);
	void SetBoundaries(CPUFluidSolver3D::BOUNDARY boundary, float* target);
	void DensityStep(float* density, float* density_prev, float* u, float* v, float* w, float diffusion);
	void VelocityStep(float* u, float* v, float* w, float* u_prev, float* v_prev, float* w_prev, float viscosity);
	void Project(float* u, float* v, float* w, float* p, float* div);

};
#endif