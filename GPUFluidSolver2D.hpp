#ifndef GPUFluidSolver2D_HPP
#define GPUFluidSolver2D_HPP

#include "glm.hpp"
#include "UniformAttributeInterfaceObject.hpp"
#include "GLBuffer.hpp"
#include "GLShader.hpp"
#include "GLUniformBuffer.hpp"
#include "CPUFluidSolver2D.hpp"



class GPUFluidSolver2D: public CPUFluidSolver2D, public UniformAttributeInterfaceObject {
public:
	GPUFluidSolver2D(const unsigned int gridSize);
	~GPUFluidSolver2D();
	void Init() override;

	void Update(double frameTime) override;
	float* GetPrevDensity() ;
	float * GetPrevVelocityX();
	float * GetPrevVelocityY();
	
	unsigned int GetDispatch();
	void SetViscosity(float);
	void SetDensity(float);

	unsigned int GetGridSize() const;
	float GetViscosity() const;
	float GetDensity() const;
	


private:	
	void SetUniformReferences() override;

	unsigned int dispatch;
	glm::vec4 data;

};





#endif // CPUWater_HPP
