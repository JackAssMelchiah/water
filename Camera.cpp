#include "Camera.hpp"


/*cstructor with default parameters:
*cameraPosition		.... [0, 0, 0]
*cameraDirection	.... [0, 0, 1]
*FOV				.... [90]
*aspectRatio		.... [16:9]
*nearFarPlane		.... [0.1, 1000]
*/
Camera::Camera(SDL_Window* w) {

	this->window = w;
	this->cameraPosition = glm::vec3(0.f);
	this->lookDirection = glm::vec3(0.f, 0.f, -1.f);

	this->fov = glm::radians(90.f);
	this->aspectRatio = 16 / 9.f;
	this->nearFarPlane = glm::vec2(0.1f, 1000.f);
	
	RecalcProjectionMatrix();
	SetUniformReferences();
}


/*Cstrusctor that allows for specifiing camera parameters*/
Camera::Camera(SDL_Window *w, glm::vec3 cameraPosition, glm::vec3 cameraDirection, float FOV, float aspectRatio, glm::vec2 nearFarPlane ) {

	this->window = w;
	this->cameraPosition = cameraPosition;
	this->lookDirection = cameraDirection;
	
	this->fov = FOV;
	this->aspectRatio = aspectRatio;
	this->nearFarPlane = nearFarPlane;

	RecalcProjectionMatrix();
	SetUniformReferences();
}


/*Saves pointers form which will be uniforms fed*/
void Camera::SetUniformReferences() {

	//UniformAttributeInterfaceObject
	SaveReference(&this->viewMatrix, sizeof(this->viewMatrix));
	SaveReference(&this->projectionMatrix, sizeof(this->projectionMatrix));
}


/*Changes view direction, allway will be normalized*/
void Camera::ChangeViewDirection(glm::vec3 dir) {

	this->lookDirection = glm::normalize(dir);
}


/*Sets postion of camera*/
void Camera::ChangePosition(glm::vec3 pos) {

	this->cameraPosition = pos;
}



/*Changes near-far plane of camera*/
void Camera::ChangeNearFarPlanes(glm::vec2 planes) {

	this->nearFarPlane = planes;
	RecalcProjectionMatrix();

}


/*Changes aspect ratio*/
void Camera::ChangeAspectRatio(float ratio) {

	this->aspectRatio = ratio;
	RecalcProjectionMatrix();
}


/*Changes fov in degrees*/
void Camera::ChangeFov(float fov) {

	this->aspectRatio = glm::radians(fov);
	RecalcProjectionMatrix();
}


/*Recalculates projection matrix, nessessary when any paramerer of it changes*/
void Camera::RecalcProjectionMatrix() {

	this->projectionMatrix = glm::perspective(fov, this->aspectRatio, this->nearFarPlane.x, this->nearFarPlane.y);
}


/*Updates ViewMatrix, required every frame if position changes*/
void Camera::UpdateCamera() {

	this->viewMatrix = glm::lookAt(this->cameraPosition, this->cameraPosition + this->lookDirection, glm::vec3(0.f,1.f,0.f));
}


/*Returns pointer to projection matrix*/
glm::mat4* Camera::GetProjection() {

	return &this->projectionMatrix;
}


/*Returns pointer to View matrix*/
glm::mat4* Camera::GetView() {

	return &this->viewMatrix;
}


/*Returns poiter to postion of camera*/
glm::vec3* Camera::GetPostion() {

	return &this->cameraPosition;
}


/*Returns poiter to direction of camera*/
glm::vec3* Camera::GetDirection() {

	return &this->lookDirection;
}


/*Returns FOV*/
float Camera::GetFoV() {

	return this->fov;
}


/*Returns window*/
SDL_Window* Camera::GetWindow() {
	
	return this->window;
}



Camera::~Camera() {}
