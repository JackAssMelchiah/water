#include "Gamepad.hpp"


Gamepad::Gamepad(SDL_GameController* controller){

	this->sdlSelf = controller;
	this->deadzoneTriggers = 0;
	this->deadzoneAxis = 0;
	this->hapticSelf = nullptr;

	//try to open it as haptic
	SDL_Haptic* haptic = SDL_HapticOpenFromJoystick(SDL_GameControllerGetJoystick(this->sdlSelf));
	if (SDL_HapticRumbleSupported(haptic) == SDL_TRUE) {
		this->hapticSelf = haptic;
		if (SDL_HapticRumbleInit(haptic) ==-1) {
			SDL_HapticClose(this->hapticSelf);
			this->hapticSelf = nullptr;
		}
	}
}

Gamepad::~Gamepad(){

	return;
	if (this->hapticSelf != nullptr) {
		SDL_HapticClose(this->hapticSelf);
	}
	if (this->sdlSelf != nullptr) {
		SDL_GameControllerClose(this->sdlSelf);
	}
}


/*Sets gamepad to rumble for specific duration in ms at %*/
void Gamepad::Rumble(float percent, int duration) {

	SDL_HapticRumblePlay(this->hapticSelf, percent, duration);
}



/*Processes axis movement
*Axis is defined by number in range <-32766;32767>
*Highest num means fully tillted to that direction
*/
void Gamepad::ProcessEvent(SDL_ControllerAxisEvent* e) {

	this->axis[(SDL_GameControllerAxis)e->axis] = e->value;
    //Utils::PrintOut(std::string("Gamepad:")+ AxisToString((SDL_GameControllerAxis)e->axis) + ":"+ std::to_string(e->value));
}


/*Processes button press*/
void Gamepad::ProcessEvent(SDL_ControllerButtonEvent* e){

    if(e->state==SDL_PRESSED){
        this->buttons[e->button]= true;
    }
    else{
        this->buttons[e->button] = false;
    }

    //Utils::PrintOut(std::string("Gamepad:BTN ") + std::to_string(e->button) + "STATE:"+ (this->buttons[e->button]?("PRESSED"):("RELEASED")));
}

/*Sets deadzone of triggers in % of the interval*/
void Gamepad::SetDeadZoneTrigger(float percent) {
	
	float p = percent;
	glm::clamp(p, 0.f, 1.f);
	this->deadzoneTriggers = int(32766.f * p);
}


/*Returns current deadzone of triggers in %*/
float Gamepad::GetDeadZoneTrigger() {

	return this->deadzoneTriggers / 32766.f;
}



/*Sets deadzone in % of the interval*/
void Gamepad::SetDeadZoneAxis(float percent) {

	float p = percent;
	glm::clamp(p, 0.f, 1.f);
	this->deadzoneAxis = int(32766.f * p);
}


/*Returns current deadzone in %*/
float Gamepad::GetDeadZoneAxis() {

	return this->deadzoneAxis / 32766.f;
}


/*Sets controller's name*/
void Gamepad::SetName(std::string n) {
	
	this->name =n;
}


/*Returns controller's name*/
std::string Gamepad::GetName() {

	return this->name;
}


/*Returns chosen axis's value in %*/
int Gamepad::GetAxisValue(Gamepad::AXIS x) {

	int value = 0;

	switch (x) {
	case Gamepad::AXIS::LEFT_X: {
		value = this->axis[SDL_CONTROLLER_AXIS_LEFTX];
		if (value < 0) { //if was to left, then subtract deadzone and clamp it in negative - min, 0
			if (value >= -this->deadzoneAxis) {
				return 0;
			}
			else {
				return int(-100 * (value + this->deadzoneAxis) / (-32766.f + this->deadzoneAxis));
			}
		}
		else { //was to right -> add deadzone and clamp in 0, max
			if (value <= this->deadzoneAxis) {
				return 0;
			}
			else {
				return int(100 * (value - this->deadzoneAxis) / (32766.f - this->deadzoneAxis));
			}
		}
		break;
	}

	case Gamepad::AXIS::LEFT_Y:{
		value = this->axis[SDL_CONTROLLER_AXIS_LEFTY];
		if (value < 0) { //if was to top?, then subtract deadzone and clamp it in negative - min, 0
			if (value >= -this->deadzoneAxis) {
				return 0;
			}
			else {
				return int(100 * (value + this->deadzoneAxis) / (-32766.f + this->deadzoneAxis));
			}
		}
		else { //was to bottom? -> add deadzone and clamp in 0, max
			if (value <= this->deadzoneAxis) {
				return 0;
			}
			else {
				return int(-100 * (value - this->deadzoneAxis) / (32766.f - this->deadzoneAxis));
			}
		}
		break;
	}

	case Gamepad::AXIS::RIGHT_X: {
		value = this->axis[SDL_CONTROLLER_AXIS_RIGHTX];
		if (value < 0) { //if was to left, then subtract deadzone and clamp it in negative - min, 0
			if (value >= -this->deadzoneAxis) {
				return 0;
			}
			else {
				return int(-100 * (value + this->deadzoneAxis) / (-32766.f + this->deadzoneAxis));
			}
		}
		else { //was to right -> add deadzone and clamp in 0, max
			if (value <= this->deadzoneAxis) {
				return 0;
			}
			else {
				return int(100 * (value - this->deadzoneAxis) / (32766.f - this->deadzoneAxis));
			}
		}
		break;
	}
	
	case Gamepad::AXIS::RIGHT_Y: {
		value = this->axis[SDL_CONTROLLER_AXIS_RIGHTY];
		if (value < 0) { //if was to top?, then subtract deadzone and clamp it in negative - min, 0
			if (value >= -this->deadzoneAxis) {
				return 0;
			}
			else {
				return int(100 * (value + this->deadzoneAxis) / (-32766.f + this->deadzoneAxis));
			}
		}
		else { //was to bottom? -> add deadzone and clamp in 0, max
			if (value <= this->deadzoneAxis) {
				return 0;
			}
			else {
				return int(-100 * (value - this->deadzoneAxis) / (32766.f - this->deadzoneAxis));
			}
		}
		break;
	}

	case Gamepad::AXIS::LEFT_TRIG:{
		value = this->axis[SDL_CONTROLLER_AXIS_TRIGGERLEFT];
		if (value <= this->deadzoneTriggers) {
			return 0;
		}
		else {
			return int(100 * (value - this->deadzoneTriggers) / (32766.f - this->deadzoneTriggers));
		}
		break;
	}

	case Gamepad::AXIS::RIGHT_TRIG: {
		value = this->axis[SDL_CONTROLLER_AXIS_TRIGGERRIGHT];
		if (value <= this->deadzoneTriggers) {
			return 0;
		}
		else {
			return int(100 * (value - this->deadzoneTriggers) / (32766.f - this->deadzoneTriggers));
		}
		break;
	}
	default:
		break;
	}

	//never reaches end
	return 0;

}



/*Translates axis enum to string*/
std::string Gamepad::AxisToString(SDL_GameControllerAxis axis){

    switch (axis){

        case(SDL_CONTROLLER_AXIS_INVALID):{
            return std::string("SDL_CONTROLLER_AXIS_INVALID");
            break;
        }
        case(SDL_CONTROLLER_AXIS_LEFTX):{
            return std::string("SDL_CONTROLLER_AXIS_LEFTX");
            break;
        }
        case(SDL_CONTROLLER_AXIS_LEFTY):{
            return std::string("SDL_CONTROLLER_AXIS_LEFTY");
            break;
        }
        case(SDL_CONTROLLER_AXIS_RIGHTX):{
            return std::string("SDL_CONTROLLER_AXIS_RIGHTX");
            break;
        }
        case(SDL_CONTROLLER_AXIS_RIGHTY):{
            return std::string("SDL_CONTROLLER_AXIS_RIGHTY");
            break;
        }
        case(SDL_CONTROLLER_AXIS_TRIGGERLEFT):{
            return std::string("SDL_CONTROLLER_AXIS_TRIGGERLEFT");
            break;
        }
        case(SDL_CONTROLLER_AXIS_TRIGGERRIGHT):{
            return std::string("SDL_CONTROLLER_AXIS_TRIGGERRIGHT");
            break;
        }
        case(SDL_CONTROLLER_AXIS_MAX):{
            return std::string("SDL_CONTROLLER_AXIS_MAX");
            break;
        }
    }
	return std::string("SDL_CONTROLLER_AXIS_INVALID");
}


