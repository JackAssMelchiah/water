#include "Buffer.hpp"


Buffer::Buffer() {

	this->freeMemoryPrt = 0; //free memory begins at 0 byte -- whole buffer is free
}


Buffer::~Buffer() {

	delete[] (char*)this->data;
}


/*Clears buffer*/
void Buffer::Clear() {

	this->freeMemoryPrt = 0;
}


/*Returs size*/
unsigned int Buffer::GetByteSize() const{

	return this->byteSize;
}


/*Initializes buffer to its size*/
bool Buffer::Initialize(unsigned int bytesize) {

	this->byteSize = bytesize;
	this->data = (void*)new char[this->byteSize];
	if (this->data == nullptr) return false;
	return true;
}


/*Returns free memory offset in bytes*/
unsigned int Buffer::GetFreeMemoryOffset() const {

	//this needs to be taken into consideration when createing MemoryPtr struct for free memory

	//first, all data are specified in bytes
	//however, the indexation within gpu buffer is not in bytes, but in 4*sizeof(bite)
	//so, to get correct memory aligment, first bytesize must be converted to gpu format
	//then it can be aligned, and then converted back

	/************example******************
	*data - 128 bytes, offset 0 -it is addresable-- 128%12 = 0, so align returns 128
	*but in gpu world this mens 128 bytes --- 32 attributes, which can be addresed if %12 ==0
	*32%12!=0 --> therefore wont work. 
	*But if first aligned in Gpu sizes, align will return 48
	*that is addresable, 48%12=0
	*which then can be converted back(now its size with padding) -- 192 bytes
	*/
	
	//this->freeMemoryPrt can be 0 so be carefull

	//return Utils::AlignMemory(this->freeMemoryPrt / sizeof(float)) * sizeof(float);

	return this->freeMemoryPrt;
}


/*Writes data to buffer, offset and size are in bytes*/
void Buffer::BufferData(void* data, int offset, int size) {

	//move end buffer if necessary
	if (offset + size > this->freeMemoryPrt) {
		this->freeMemoryPrt = offset + size;
	}
	
	////DEBUG
	//int float_size = sizeof(float);
	//int count = size / float_size;
	//std::string debug = "\n*Buffering values: " + std::to_string(count) + "*\n";
	//for (int i = 0; i < count; ++i) {
	//	debug+=std::to_string(*(((float*)data) + i))+" ";
	//}
	//Utils::PrintOut(debug);
	////DEBUG out

	void* dest_ptr = (void*)(((char*)this->data) + offset);
	memcpy(dest_ptr, data, size);
}


void* Buffer::GetBufferMemoryToRead(int offset) const {

	return (void*)(((char*)this->data) + offset);
}