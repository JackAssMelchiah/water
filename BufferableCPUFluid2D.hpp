#ifndef BUFFERABLE_CPU_FLUID_2D
#define BUFFERABLE_CPU_FLUID_2D

#include "GPUBufferableInterface.hpp"
#include "CPUFluidSolver2D.hpp"
#include "GLPipelineState.hpp" 

//TODO FACTORY FOR RENDERER - THIS ONE ABSTRACT

class BufferableCPUFluid2D: public GPUBufferableInterface, public CPUFluidSolver2D {
public:
	BufferableCPUFluid2D(const unsigned int dimmension);
	virtual ~BufferableCPUFluid2D();
	
	bool Initialize(GLBuffer* ssbo, GLBuffer* genericBuffer, const unsigned int ssboOffset, const unsigned int genericOffset) override;
	void Update(double frameTime) override;
	GLPipelineState* GetPipelineStateSettings() override;
	void GetShaders(std::vector<GLShader*>& shaders) override;
	void GetAllVertexFormats(std::vector<GLRenderFormat::VertexFormat>& formats) override;
	unsigned int GetReqMemorySSBO() override;
	unsigned int GetReqMemoryGenericBuffer() override;
	void GetIncompleteDrawCommand(InternalRenderFormat::IncopleteCommand& comand) override;

protected:
	void CreatePipelineState();
	bool CreateShaders();

private:
	void SetUniformReferences() override;

	GLPipelineState pipelineState;

	GLBuffer* boundVBO;
	unsigned int boundVBOOffset;

	GLBuffer* boundSSBO;
	unsigned int boundSSBOOffset;

	GLShader* boundShader;
};










#endif // ! BUFFERABLE_CPU_FLUID_2D
