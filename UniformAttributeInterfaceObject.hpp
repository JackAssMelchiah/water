#ifndef UNIFORM_ATTRIB_INTERF_OB_HPP
#define UNIFORM_ATTRIB_INTERF_OB_HPP

#include <vector>

class UniformAttributeInterfaceObject {
public:
	UniformAttributeInterfaceObject();
	virtual ~UniformAttributeInterfaceObject();
	int GetReferenceCount() const;
	void* GetRefPtr(int) const;
	int GetRefSize(int) const;
protected:
	virtual void SetUniformReferences() = 0;
	void SaveReference(void*, int);
private:
	std::vector<std::pair<int, void* const>> references;
};




#endif // !UNIFORM_ATTRIB_INTERF_OB_HPP
