#ifndef GL_UNIFORM_BUFFER_HPP
#define GL_UNIFORM_BUFFER_HPP

#include "GLBuffer.hpp"
#include "GLUniformBlock.hpp"
#include <optional>

class GLUniformBuffer: public GLBuffer {
public:
	GLUniformBuffer();
	~GLUniformBuffer();

	void UseBufferForRender(unsigned int offset, unsigned int size) const;
	void UseBufferForRender() const override;
	void UnuseBufferForRender() const override;
	void SetUpShaderBinding(GLuint bindingPoint, GLuint shaderBinding);
	void BufferData(GLUniformBlock*, int globalOffset);
private:
	std::optional<GLint> bindPoint;
	std::optional<GLint> shaderProgram;
};







#endif // !GL_UNIFORM_BUFFER_HPP


