#ifndef _INT_REN_FOR_
#define _INT_REN_FOR_

#include <vector>
#include <list>

#ifdef RENDERER_GL
#include "GL/glew.h"
#else
typedef uint32_t GLuint;
#endif // RENDERER_GL

#include "glm.hpp"
#include "gtc/type_ptr.hpp"
#include "Utils.hpp"

class InternalRenderFormat {
public:

	enum class DATA_TYPE { FLOAT, VEC2, VEC3, VEC4, MAT4 };
	enum class BUFFER_TYPE { FAST, NORMAL };

	typedef struct {
		GLuint count;
		GLuint instanceCount;
		GLuint first;
		GLuint baseInstance;
	}DrawCommand;

	typedef struct {
		GLuint count;
		GLuint instanceCount;
	}IncopleteCommand;

	typedef struct {
		int id;
		DATA_TYPE type;
		int divisor;
		int length;
		int numMatrixColls;
		unsigned int binding_point;
		void* data;
	}Metadata;

	//this one has just info about perInst or generic data within, its possible that its not needed
	typedef struct {
		Metadata* rawPtrsMetadata;
		bool perInstance;
	}ValueInfoPtr;

	typedef struct {
		int byteSize;
		void* dataPtr;
	} UniformBlockMetadata;

	typedef struct {
		int offset;
		int size;
	}GPUMemoryPtr;


	static void CompleteDrawCommand(const InternalRenderFormat::IncopleteCommand& ic, InternalRenderFormat::DrawCommand& c, unsigned int baseOffset, unsigned int baseInstance);
	friend bool operator==(const InternalRenderFormat::Metadata& data, const InternalRenderFormat::Metadata& data2);
	static int DataTypeConsistsOf(DATA_TYPE type);
	static void FreeMetadata(Metadata&);
	static std::string MetadataToString(InternalRenderFormat::Metadata*);
};


#endif // ! _INT_REN_FOR_
