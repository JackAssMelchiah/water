#include "PipelineState.hpp"

/*At creation will set the pipeline state to default state*/
PipelineState::PipelineState() {
	
	SetDefaultState();
}

PipelineState::~PipelineState() {

}

/*Sets pipeline to defaults except for viewport*/
void PipelineState::SetDefaultState() {
	
	SetCulling(false, CULL_MODE::BACK_F);
	SetFrontFace(FRONT_FACE::CCLOCKWISE);
	SetPolygonMode(POLYGON_MODE::FILL);
	SetRasterize(true);
	SetTopology(TOPOLOGY::TRIANGLE);
	SetDepthTest(true, DEPT_TEST_FUNC::LESS);
	SetBlending(false, BLENDING_FUNC::ZERO, BLENDING_FUNC::ZERO);
	
	this->lineWidth = 1.f;
	this->multisampling = 1;

	this->initialized = true;
}


/*Specifies blending and mixing functions*/
void PipelineState::SetBlending(bool enabled, BLENDING_FUNC src, BLENDING_FUNC dst) {

	this->Blending.blendingDst = dst;
	this->Blending.blendingSrc = src;
	this->Blending.enabled = enabled;
}


/*Enables|Disables depth test, and specifies depth test function*/
void PipelineState::SetDepthTest(bool enabled, DEPT_TEST_FUNC func) {

	this->DepthTest.depthTestFunction = func;
	this->DepthTest.enabled = enabled;
}


/*Sets topology for rendering*/
void PipelineState::SetTopology(TOPOLOGY topology) {

	this->topology = topology;
}


/*Sets whether polygons should be filled or wireframe*/
void PipelineState::SetPolygonMode(POLYGON_MODE polymode) {

	this->polygonMode = polymode;
}


/*Sets whether culling should be enabled and what faces should be affected*/
void PipelineState::SetCulling(bool enabled, CULL_MODE cullmode) {

	this->FaceCull.cullMode = cullmode;
	this->FaceCull.enabled = enabled;
}


/*Allows to specify normal orientation for vertices, can be specified to be set as clockwise or counter-clockwise */
void PipelineState::SetFrontFace(FRONT_FACE frontFace) {

	this->frontFace = frontFace;
}


/*Sets whether rasterization is on*/
void PipelineState::SetRasterize(bool enable) {

	this->Rasterization.enabled = enable;
}
