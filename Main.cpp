#define NO_SDL_GLEXT
#define DEBUG

/*Visual leak detector*/
//#include <vld.h>


#ifndef RESOURCE_DIR
#define RESOURCE_DIR "Resources/"
#endif

#ifndef SHADER_DIR
#define SHADER_DIR "Shaders/"
#endif



#include <SDL.h>
#ifdef RENDERER_GL
#include <GL/glew.h>
#include <SDL_opengl.h>
#endif
#include <SDL_image.h>
#include <string>
#include "WaterRenderer.hpp"
#include "Utils.hpp"


/* ** * **  * * ** *
* EDITABLE PARAMS
* * * * ** * * * * */
//! simulation grid size, must be multiple of 32, for 2D 64, 128, 256 is recommended, for 32 or 64 is recommended for 3D
#ifndef GRID
	#define GRID 64
#endif
//! renderer type, 0: 2D renderer, 1: 3D renderer, 2: 2D accelerated renderer on GPU, currently not fully supported
#ifndef RENDERER_TYPE
	#define RENDERER_TYPE 0
#endif
/* ** * **  * * ** *
* EDITABLE PARAMS
* * * * ** * * * * */


int main(int, char* args[]);
void Close();
bool Init();


SDL_GLContext glContext;
SDL_Window* Window = NULL;

//water adds internaly 2 to both dimmensions, and result needs to be dividable by 32
unsigned int waterRows = GRID - 2;

int main(int argc, char* args[]) {
	
	if ((waterRows + 2)  % 32 != 0) {
		Utils::PrintOut("Grid must be of size dividable by 32, press Enter to exit");
		getchar();
		return 0;
	}

	if (!Init()) {
		Utils::PrintOutErr("Failed to initialize!");
		Close();
		return 1;
	}

	WaterRenderer* renderer = new WaterRenderer(Window, waterRows);
	bool retval = renderer->Init(static_cast<FluidSolverFactory::TYPE>(RENDERER_TYPE));
	assert(retval);
	renderer->Loop();
	delete renderer;
	Close();	
	return 0;
}



/*Initializes SDL*/
bool Init() {

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		Utils::PrintOutErr("SDL init err");
		return false;
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	Window = SDL_CreateWindow("Water", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE); // | SDL_WINDOW_FULLSCREEN_DESKTOP


	//init SLD IMAGE 
	int flags = IMG_INIT_JPG | IMG_INIT_PNG;
	int init_img = IMG_Init(flags);
	if (init_img & flags != flags) {
		Utils::PrintOutErr("cannot init JPG&PNG! Sorry.");
		return false;
	}

	//context
	glContext = SDL_GL_CreateContext(Window);
	if (glContext == nullptr) {
		Utils::PrintOutErr("glcontext err");
		return false;
	}

	//glew
	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();

	if (glewError != GLEW_OK) {
		Utils::PrintOutErr("glew init err");
		return false;
	}

	return true;
}



/*Close*/
void Close(){

	SDL_DestroyWindow(Window);
	Window = nullptr;
	IMG_Quit();
	SDL_Quit();
}
