#ifndef GL_DRAW_CALLS_BUFFER_HPP
#define GL_DRAW_CALLS_BUFFER_HPP

#include "GLBuffer.hpp"
#include "InternalRenderFormat.hpp"

class GLDrawCallsBuffer: public GLBuffer {
public:
	GLDrawCallsBuffer();
	~GLDrawCallsBuffer();				
	bool Initialize(unsigned int bytesize);												//OVERRIDE Buffer::Initialize
	void UseBufferForRender() const override;															//OVERRIDE GLBufferInterface::UseBufferForRender
	void UnuseBufferForRender() const override;														//OVERRIDE GLBufferInterface::UnuseBufferForRender
	void OpenBuffer() override;																	//OVERRIDE GLBuffer::OpenBuffer
	void* GetBufferMemoryToRead() const;														//OVERRIDE GLBuffer::GetBufferMemoryToRead
	int QuerryGPUBufferSize();															//OVERRIDE GLBufferInterface::QuerryGPUBufferSize
	void BufferData(InternalRenderFormat::DrawCommand*, int count, int unit_offset);	//OVERRIDE Buffer::BufferData
private:
};
#endif // !GL_DRAW_CALLS_BUFFER_HPP
