#include "FpsCamera.hpp"

/*cstructor with default parameters:
*cameraPosition		.... [0, 0, 0]
*cameraDirection	.... [0, 0, 1]
*FOV				.... [90]
*aspectRatio		.... [16:9]
*nearFarPlane		.... [0.1, 1000]
*/
FpsCamera::FpsCamera(SDL_Window* w):Camera(w) {

	this->pitchAngle = 0;
	this->yawAngle = 0;
}


/*Secondary cstructor*/
FpsCamera::FpsCamera(SDL_Window* w, glm::vec3 cameraPosition, glm::vec3 cameraDirection, float FOV, float aspectRatio, glm::vec2 nearFarPlane) :Camera(w, cameraPosition, cameraDirection, FOV, aspectRatio, nearFarPlane) {
	
	this->yawAngle = 0.f;
	this->pitchAngle = 0.f;
}


/*Moves camera up/down, expects allready framerate independent and acceleration modified value for distance*/
void FpsCamera::MoveUpDown(float distance) {

	ChangePosition(*GetPostion() + distance * glm::normalize(glm::cross(glm::normalize(glm::cross(glm::vec3(0.f, 1.f, 0.f), *GetDirection())),*GetDirection())));
}


/*Camera strafes left(-) or right(+), expects allready framerate independent and acceleration modified value for distance*/
void FpsCamera::StrafeLeftRight(float distance) {

	ChangePosition(*GetPostion() + glm::normalize(glm::cross(glm::vec3(0.f, 1.f, 0.f), *GetDirection())) * distance);
}


/*Camera moves forward(+) or backward(-), expects allready framerate independent and acceleration modified value for distance*/
void FpsCamera::MoveForwardBackward(float distance) {
	
	ChangePosition(*GetPostion() + distance * *GetDirection());
}


/*Rotates camera along X axis in certain angle in degrees, expects allready framerate independent and acceleration modified value for angle*/
void FpsCamera::ChangeCameraPitch(float angle) {
	
	//check so camera does not oversnap 
	if (this->pitchAngle - angle > 80.f || this->pitchAngle - angle < -80.f) 
		return;
	
	this->pitchAngle -= angle;

	glm::vec3* viewDirection = GetDirection();
	ChangeViewDirection(glm::mat3(glm::rotate(glm::radians(angle), glm::cross(glm::vec3(0.f, 1.f, 0.f), *viewDirection))) * (*viewDirection));
}


/*Rotates camera along Y axis(Z blender's) in certain angle in degrees, expects allready framerate independent and acceleration modified value for angle*/
void FpsCamera::ChangeCameraYaw(float angle) {

	this->yawAngle += angle;

	glm::vec3* viewDirection = GetDirection();
	ChangeViewDirection(glm::mat3(glm::rotate(glm::radians(angle), glm::vec3(0.f, 1.f, 0.f))) * (*viewDirection));
}


/*Sets postition of camera and direction which its facing -- this is useful when need to change camera position completely*/
void FpsCamera::SetPostion(glm::vec3 position, glm::vec3 viewDirection) {

	ChangePosition(position);
	ChangeViewDirection(viewDirection);
}



FpsCamera::~FpsCamera(){}
