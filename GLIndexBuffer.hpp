#ifndef GL_INDEX_BUFFER_HPP
#define GL_INDEX_BUFFER_HPP

#include "GLBuffer.hpp"
#include "Utils.hpp"


class GLIndexBuffer: public GLBuffer {
public:
	GLIndexBuffer();
	virtual ~GLIndexBuffer() override;
	virtual void UseBufferForRender() const override;
	virtual void UnuseBufferForRender() const override;
	virtual int QuerryGPUBufferSize() const override;
};






#endif // !GL_BUFFER_HPP
