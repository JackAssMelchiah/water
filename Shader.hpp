#ifndef SHADER_HPP
#define SHADER_HPP


#include <string>

#include "Utils.hpp"



class Shader{
public:
	typedef int IDSHADER;
	friend bool operator==(const Shader& data, const Shader& data2);

	Shader();
	virtual ~Shader() {}
	virtual bool LoadShader(string computeShaderPath) = 0;
	virtual bool LoadShader(string vertexShaderPath, string fragmentShaderPath) = 0;
	virtual bool LoadShader(string vertexShaderPath, string geometryShaderPath, string fragmentShaderPath) = 0;
	virtual bool LoadShader(string vertexShaderPath, string tessalationControllShaderPath, string tessalationEvaluationShaderPath, string fragmentShaderPath) = 0;
	virtual bool LoadShader(string vertexShaderPath, string tessalationControllShaderPath, string tessalationEvaluationShaderPath, string geometryShaderPath, string fragmentShaderPath) = 0;


	IDSHADER GetID() const;
	bool IsShaderLoaded() const;

protected:
	enum class ShaderType { VERTEX, FRAGMENT, GEOMETRY, T_CONTROL, T_EVALUATUION, COMPUTE };
	void SetShaderLoaded();
	virtual bool ShaderFromFile(string shaderPath, std::vector<char>* shaderHolder);

private:
	static int _SHADER_COUNT_; 
	bool loaded_shader = false;
	IDSHADER id;
};


#endif

