#ifndef FLUID_SOLVER_FACTORY
#define FLUID_SOLVER_FACTORY

#include "BufferableCPUFluid2D.hpp"
#include "BufferableGPUFluid2D.hpp"
#include "BufferableCPUFluid3D.hpp"
#include "GLBufferInterface.hpp"

class FluidSolverFactory {

public:
	enum class TYPE { CPU_2D, CPU_3D, GPU_2D };
	static GPUBufferableInterface* NewSolver(TYPE, const unsigned int);

private:

};



#endif // !FLUID_SOLVER_FACTORY
