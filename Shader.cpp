#include "Shader.hpp"

int Shader::_SHADER_COUNT_ = 0;

/*==*/
bool operator==(const Shader & data, const Shader & data2) {
	
	if (data.GetID() == data2.GetID()) {
		return true;
	}
	return false;
}


Shader::Shader() {
	this->id = Shader::_SHADER_COUNT_;
	Shader::_SHADER_COUNT_++;
}


/*Getter for ID*/
Shader::IDSHADER Shader::GetID() const{
	return this->id;
}


/*Returns if the shader status is loaded*/
bool Shader::IsShaderLoaded() const {
	return this->loaded_shader;
}


/*Sets state to shader loaded*/
void Shader::SetShaderLoaded() {
	this->loaded_shader = true;
}


/*Method opens the file, and returns NULL terminated array with all text in it */
bool Shader::ShaderFromFile(string shaderPath, std::vector<char>* shaderHolder) {

	shaderHolder->clear();
	FILE* f;
	f = fopen(shaderPath.c_str(), "r");
	if (f == NULL) {
		return false;
	}
	int c;
	while (c = fgetc(f)) {
		if (c == EOF)
			break;
		shaderHolder->push_back(c);
	}
	fclose(f);
	return true;
}