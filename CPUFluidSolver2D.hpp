#ifndef CPUFluidSolver2D_HPP
#define CPUFluidSolver2D_HPP

#include <string>
#include "glm.hpp"
#include "Utils.hpp"
#include "FluidSolverInterface.hpp"


class CPUFluidSolver2D: public FluidSolverInterface {
public:
	
	CPUFluidSolver2D(const unsigned int gridSize);
	virtual ~CPUFluidSolver2D();

	void Init() override;
	void Update(double frameTime) override;
	bool CAPTURESTATE() override;
	bool LOADSTATE() override;

	float * GetVelocityX();
	float * GetVelocityY();
	float * GetDensity();
	void AddDensity(glm::ivec2 coords, float flow);
	void AddForce(glm::ivec2 coords, float force);
	void ULTIMATEDEBUG(float * p);
	void KeepSourcesConstant();

	std::string DEBUGSTRING(float* vec);

protected:

	float* u;
	float* v;
	float* density;
	float* vPrev;
	float* uPrev;
	float* densityPrev;
	double frameTime = 0.01667;
	const unsigned int gridSize;

private:
	enum class BOUNDARY {NONE, HORIZONTAL, VERTICAL };

	void AddSource(float* target, float* source);
	void Diffuse(BOUNDARY boundarySelection, float* src, float* src_prev, float diffuse);
	void Advect(BOUNDARY boundary, float* density, float* density_prev, float* u, float* v);
	void SetBoundaries(CPUFluidSolver2D::BOUNDARY boundary, float* target);
	void DensityStep(float* density, float* density_prev, float* u, float* v, float diffusion);
	void VelocityStep(float* u, float* v, float* u_prev, float* v_prev, float viscosity);
	void Project(float* u, float* v, float* p, float* div);
	
};




#endif // CPUFluidSolver2D_HPP
