#include "BufferableCPUFluid3D.hpp"

BufferableCPUFluid3D::BufferableCPUFluid3D(const unsigned int dimmension): CPUFluidSolver3D(dimmension){

	this->dimension = glm::ivec4(dimmension + 2);
	this->dimension.w = 1;

	SetUniformReferences();
}


BufferableCPUFluid3D::~BufferableCPUFluid3D() {
}


/*Initialization, prepares pipeline and shaders */
bool BufferableCPUFluid3D::Initialize(GLBuffer * ssbo, GLBuffer * genericBuffer, const unsigned int ssboOffset, const unsigned int genericOffset) {
	
	//not using one
	this->boundVBO = genericBuffer;
	this->boundVBOOffset = genericOffset;

	this->boundSSBO = ssbo;
	this->boundSSBOOffset = ssboOffset;
	
	//simulation initialization
	Init();

	//pipeline creation
	CreatePipelineState();

	//create grid
	std::vector<glm::vec3> cube;
	std::vector<glm::vec3> cubeCol;
	Shape::Cube(cube, cubeCol);

	//one time buffering of the cube grid 
	genericBuffer->OpenBuffer();
	genericBuffer->BufferData(&cube.data()->x, genericOffset, cube.size() * sizeof(glm::vec3));
	genericBuffer->CloseBuffer();

	//shader load
	return CreateShaders();
}


/*Ticks every frame, must upload new data and perform simulation update*/
void BufferableCPUFluid3D::Update(double frameTime) {

	unsigned int size = (this->gridSize + 2) * (this->gridSize + 2)  * (this->gridSize + 2) * sizeof(float);
	unsigned int attrib_start_offset = boundSSBOOffset;

	CPUFluidSolver3D::Update(frameTime); //updates the fluid simulation
	
	this->boundSSBO->OpenBuffer();
	this->boundSSBO->BufferData(this->GetDensity(), attrib_start_offset, size);
	this->boundSSBO->CloseBuffer();
}


/*Returns pipeline state*/
GLPipelineState * BufferableCPUFluid3D::GetPipelineStateSettings() {
	
	return &this->pipelineState;
}


/*Returns format of the water grid*/
void BufferableCPUFluid3D::GetAllVertexFormats(std::vector<GLRenderFormat::VertexFormat>& formats) {

	formats.clear();

	GLRenderFormat::VertexFormat format;
	format.bufferHandle = this->boundVBO->GetBufferHandle();
	format.bindPoint = 0;
	format.dataConsistOf = 3;
	format.bufferOffset = 0; //boundVBOOffset
	format.stride = 3 * sizeof(float);
	format.divisor = 0; //how many instances will pass between updates of gen. attribute at binding point

	formats.push_back(format);
}


/*Returns memory requirements for ssbo*/
unsigned int BufferableCPUFluid3D::GetReqMemorySSBO() {
	
	return (this->gridSize + 2) * (this->gridSize + 2) * (this->gridSize + 2) * sizeof(float);
}


/*Returns memory requirements for generic memory buffer*/
unsigned int BufferableCPUFluid3D::GetReqMemoryGenericBuffer() {
	
	return sizeof(glm::vec3) * 36;
}


/*Returns pipeline state*/
void BufferableCPUFluid3D::GetShaders(std::vector<GLShader*>& shaders) {

	shaders.clear();
	shaders.push_back(this->boundShader);
}


/*Returns incomplete draw command*/
void BufferableCPUFluid3D::GetIncompleteDrawCommand(InternalRenderFormat::IncopleteCommand & command) {

	command.instanceCount = (this->gridSize + 2) * (this->gridSize + 2) * (this->gridSize + 2);
	command.count = 36; //vertex count of noninstanced model
}


/*Bulds pipeline, with which its able to be rendered with*/
void BufferableCPUFluid3D::CreatePipelineState() {

	//glEnable(GL_DEPTH_TEST) and glDepthMask(GL_FALSE)
	this->pipelineState.SetDepthTest(false, PipelineState::DEPT_TEST_FUNC::LESS);
	this->pipelineState.SetBlending(true, PipelineState::BLENDING_FUNC::ONE, PipelineState::BLENDING_FUNC::ONE_MINUS_SRC_ALPHA);
	this->pipelineState.SetCulling(false, PipelineState::CULL_MODE::BACK_F);
	this->pipelineState.SetFrontFace(PipelineState::FRONT_FACE::CLOCKWISE);
	this->pipelineState.SetPolygonMode(PipelineState::POLYGON_MODE::FILL);
	this->pipelineState.Build();
}


/*Load all shaders used within this class*/
bool BufferableCPUFluid3D::CreateShaders() {
	
	this->boundShader = new GLShader();

	if (!this->boundShader->LoadShader(std::string(SHADER_DIR) + "fluid3D.vert", std::string(SHADER_DIR) + "fluid3D.frag")) return false;
	return true;
}


/*Saves pointers to uniforms*/
void BufferableCPUFluid3D::SetUniformReferences() {

	SaveReference(&this->dimension, sizeof(this->dimension));
}
