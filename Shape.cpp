#include "Shape.hpp"


/*Generates Axies mesh and colors*/
void Shape::Axies(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>&colors) {

	/*Creates 3 Cubes, then rotates them*/
	vertices.clear();
	colors.clear();

	Shape::ParametricCube(vertices, 1.f, .1f, .1f, glm::vec3(-1.f, -1.f, 0.f));
	Shape::ParametricCube(vertices, .1f, 1.f, .1f, glm::vec3(-1.f, -1.f, 0.f));
	Shape::ParametricCube(vertices, .1f, .1f, 1.f, glm::vec3(-1.f, -1.f, -1.f));

	for (int i = 0; i < vertices.size() / 3; ++i) {
		colors.push_back(glm::vec3(1.f, 0.f, 0.f));
	}
	for (int i = 0; i < vertices.size() / 3; ++i) {
		colors.push_back(glm::vec3(0.f, 0.f, 1.f));
	}
	for (int i = 0; i < vertices.size() / 3; ++i) {
		colors.push_back(glm::vec3(0.f, 1.f, 0.f));
	}
}


/*Generates cube mesh and colors*/
void Shape::Cube(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>&colors) {

	vertices.clear();
	colors.clear();
	Shape::ParametricCube(vertices, 1, 1, 1, glm::vec3(0.f, -1.f, 0.f));
	for (auto vertex : vertices) {
		colors.push_back(glm::vec3(.5f));
	}
}


/*Returns cube as vertices, given by with heigh depth params, as well as given by origin([-1-,1,-1]is left corner, [-1-,1,-1] is right corner)   */
void Shape::ParametricCube(std::vector<glm::vec3>&vertices, float width, float height, float depth, glm::vec3 unitOrigin) {

	glm::vec3 origin = glm::clamp(unitOrigin, glm::vec3(-1.f), glm::vec3(1.f)) * glm::vec3(width, height, depth) / 2.f;

	glm::vec3 minVal(-width / 2.f, -height / 2.f, -depth / 2.f);
	glm::vec3 maxVal(width / 2.f, height / 2.f, depth / 2.f);
	int start_pos = vertices.size();

	//front face
	vertices.push_back(glm::vec3(minVal.x, minVal.y, minVal.z));
	vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));
	vertices.push_back(glm::vec3(maxVal.x, minVal.y, minVal.z));

	vertices.push_back(glm::vec3(maxVal.x, minVal.y, minVal.z));
	vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));
	vertices.push_back(glm::vec3(maxVal.x, maxVal.y, minVal.z));

	//back face
	vertices.push_back(glm::vec3(minVal.x, minVal.y, maxVal.z));
	vertices.push_back(glm::vec3(minVal.x, maxVal.y, maxVal.z));
	vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));

	vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));
	vertices.push_back(glm::vec3(minVal.x, maxVal.y, maxVal.z));
	vertices.push_back(glm::vec3(maxVal.x, maxVal.y, maxVal.z));

	//left face
	vertices.push_back(glm::vec3(minVal.x, minVal.y, minVal.z));
	vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));
	vertices.push_back(glm::vec3(minVal.x, minVal.y, maxVal.z));

	vertices.push_back(glm::vec3(minVal.x, minVal.y, maxVal.z));
	vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));
	vertices.push_back(glm::vec3(minVal.x, maxVal.y, maxVal.z));

	//right face
	vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));
	vertices.push_back(glm::vec3(maxVal.x, maxVal.y, minVal.z));
	vertices.push_back(glm::vec3(maxVal.x, minVal.y, minVal.z));

	vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));
	vertices.push_back(glm::vec3(maxVal.x, maxVal.y, maxVal.z));
	vertices.push_back(glm::vec3(maxVal.x, maxVal.y, minVal.z));

	//top face
	vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));
	vertices.push_back(glm::vec3(minVal.x, maxVal.y, maxVal.z));
	vertices.push_back(glm::vec3(maxVal.x, maxVal.y, maxVal.z));

	vertices.push_back(glm::vec3(maxVal.x, maxVal.y, maxVal.z));
	vertices.push_back(glm::vec3(maxVal.x, maxVal.y, minVal.z));
	vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));

	//bottom face
	vertices.push_back(glm::vec3(minVal.x, minVal.y, minVal.z));
	vertices.push_back(glm::vec3(minVal.x, minVal.y, maxVal.z));
	vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));

	vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));
	vertices.push_back(glm::vec3(maxVal.x, minVal.y, minVal.z));
	vertices.push_back(glm::vec3(minVal.x, minVal.y, minVal.z));

	for (int i = start_pos; i < vertices.size(); ++i) {
		vertices[i]+= origin;
	}
}


/*Returns grid within <-1,1> consisting from desired number of points */
void Shape::Grid(std::vector<glm::vec3>& vertices, unsigned int rowPointsCount) {

	vertices.clear();
	float delta = 2.f / rowPointsCount;
	glm::vec3 center;
	glm::vec3 TL(-delta/2.f, 0.f, -delta / 2.f);
	glm::vec3 TR(+delta/2.f, 0.f, -delta / 2.f);
	glm::vec3 BL(-delta / 2.f, 0.f, +delta /2.f);
	glm::vec3 BR(+delta / 2.f, 0.f, +delta / 2.f);

	for (int i = 0; i < rowPointsCount; ++i) {
		for (int j = 0; j < rowPointsCount; ++j) {
			
			center = glm::vec3((i*delta + delta/ 2.f) -1.f , 0.f, (j*delta + delta / 2.f) -1.f );
			
			vertices.push_back(center + TL);
			vertices.push_back(center + BL);
			vertices.push_back(center + TR);

			vertices.push_back(center + TR);
			vertices.push_back(center + BL);
			vertices.push_back(center + BR);
		}
	}
}