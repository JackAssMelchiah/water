#include "Utils.hpp"
#include "lib\headers\tinyDir\tinydir.h"


std::string Utils::VecToStr(glm::vec4 v) {

	string s = "[" + std::to_string(v.x) + ", " + std::to_string(v.y) + ", " + std::to_string(v.z) + ", " + std::to_string(v.w) + "]";
	return s;
}

std::string Utils::VecToStr(glm::vec3 v) {

	string s = "[" + std::to_string(v.x) + ", " + std::to_string(v.y) + ", " + std::to_string(v.z) + "]";
	return s;
}

std::string Utils::VecToStr(glm::vec2 v) {

	string s = "[" + std::to_string(v.x) + ", " + std::to_string(v.y) + "]";
	return s;
}


/*vec3 LERP*/
glm::vec3 Utils::Interpolate(glm::vec3& A, glm::vec3& B, float t) {

	return B*t + A*(1.f - t);
}


/* Quat Slerp*/
glm::quat Utils::Interpolate(glm::quat& A, glm::quat& B, float t) {

	return glm::slerp(A, B, t);
}


float Utils::Interpolate(float A, float B, float t) {

	return (B-A)*t + A;
}


/*Splits input string by string delimeter, returns it in 3rd parameter*/
void Utils::SplitString(string target, string split_by, std::vector<string>& ret) {
	
	int s_c_pos = 0;
	int s_l_pos = 0;
	bool f = true;

	//find first combination
	s_c_pos = target.find(split_by, s_l_pos);
	if (s_c_pos == -1) {
		return;
	}
	ret.insert(ret.begin() + ret.size(), target.substr(s_l_pos, s_c_pos));
	s_l_pos = s_c_pos;
	//find the rest

	while (1) {
		s_c_pos = target.find(split_by, ++s_l_pos);
		if (s_c_pos == -1) {
			if (s_l_pos != 0) {
				ret.insert(ret.begin() + ret.size(), target.substr(s_l_pos + split_by.length() - 1, target.length() - s_l_pos));
			}
			break;
		}
		ret.insert(ret.begin() + ret.size(), target.substr(s_l_pos + split_by.length() - 1, s_c_pos - s_l_pos - split_by.length() +1));
		s_l_pos = s_c_pos;
		f = false;
	}
}


/*Returns width and height of window*/
glm::ivec2 Utils::GetWindowResolution(SDL_Window* window) {
	
	glm::ivec2 res;
	Utils::GetWindowResolution(window, &res.x, &res.y);
	return res;
}


/*Returns width and height of window*/
void Utils::SetWindowResolution(SDL_Window* window, glm::ivec2 res) {

	Utils::SetWindowResolution(window, res.x, res.y);
}


/*Returns width and height of window*/
void Utils::GetWindowResolution(SDL_Window* window, int* width, int* height) {

	SDL_GetWindowSize(window, width, height);
}


/*Returns width and height of window*/
void Utils::SetWindowResolution(SDL_Window* window, int width, int height) {

	SDL_SetWindowSize(window, width, height);
}



/*Range convertor*/
float Utils::ConvertRange(float curr_val, float old_min, float old_max, float new_min, float new_max) {

	return (((curr_val - old_min) * (new_max - new_min)) / (old_max - old_min)) + new_min;

}


/*Returns cstring with full path to resource*/
void Utils::GetFullResourcePath(string& abs_path) {

	abs_path = RESOURCE_DIR + abs_path;
}


/*Returns cstring with full path to shader*/
void Utils::GetFullShaderPath(string& abs_path) {

	abs_path = SHADER_DIR + abs_path;
}


/*Indexes vectors according to indices*/
void Utils::DoIndexing(std::vector<glm::vec3>& vertices, std::vector<glm::vec2>& uvs, std::vector<glm::vec3>& normals, std::vector<int>& indices, std::vector<glm::vec3>& indexed_vert, std::vector<glm::vec2>& indexed_uvs, std::vector<glm::vec3>& indexed_normals) {

	for (unsigned long i = 0; i < indices.size(); ++i) {
		long index = indices[i];

		glm::vec3 indexed_vertex = vertices[index];
		glm::vec2 indexed_uv = uvs[index];
		glm::vec3 indexed_normal = normals[index];

		indexed_vert.push_back(indexed_vertex);
		indexed_uvs.push_back(indexed_uv);
		indexed_normals.push_back(indexed_normal);
	}
}



/*Writes into new file, if it exist, its emptied*/
bool Utils::WriteToClearFile(std::string path, void* data, unsigned int elementByteSize, unsigned int length) {

	FILE* f = fopen(path.c_str(), "wb+");
	if (f == nullptr) return false;
	fwrite(data, elementByteSize, length, f);
	fclose(f);
	return true;
}


/*Writes into new file, if it exist, data are apended to it */
bool Utils::WriteToExistingFile(std::string path, void* data, unsigned int elementByteSize, unsigned int length) {

	FILE* f = fopen(path.c_str(), "ab");
	if (f == nullptr) return false;
	fwrite(data, elementByteSize, length, f);
	fclose(f);
	return true;
}


/*Reads whole file into data, will allocate data, and return them in it, and return lenght
* requires element byte size to be specfied
*/
bool Utils::ReadFromFile(std::string path, int expectedElementize, void** data, unsigned int& lenght) {

	FILE *f = fopen(path.c_str(), "r");
	if (f == nullptr) return false;
	fseek(f, 0L, SEEK_END);
	unsigned int bytesize = ftell(f);

	//allocated enough data for pointer
	*data = new char[bytesize];
	lenght = bytesize / expectedElementize;
		
	rewind(f);

	fread(*data, lenght, expectedElementize, f);
	fclose(f);
	return true;
}


/*Error print function, prints into log if exist, else into stdout
* To create log, use Utils::NewLogger()
* To close logger, use Utils::ClodeLogger
*/
void Utils::PrintOutErr(std::string msg) {

	#ifdef PLATFORM_WINDOWS
	std::cout << "(00:00:00)[ERROR]:" << msg << std::endl;
	#else
	std::cout << "\033[1;31m[](00:00:00)[ERROR]:\033[0m" << msg << std::endl;
	#endif // PLATFORM_WINDOWS
}


/*Warning print function, prints into log if exist, else into stdout
* To create log, use Utils::NewLogger()
* To close logger, use Utils::ClodeLogger
*/
void Utils::PrintOutWar(std::string msg) {

	#ifdef PLATFORM_WINDOWS
	std::cout << "(00:00:00)[WARNING]:" << msg << std::endl;
	#else
	std::cout << "\033[1;33m[](00:00:00)[WARNING]:\033[0m" << msg << std::endl;
	#endif // PLATFORM_WINDOWS
}


/*Standard print function, prints into log if exist, else into stdout
* To create log, use Utils::NewLogger()
* To close logger, use Utils::ClodeLogger
*/
void Utils::PrintOut(std::string msg) {

	#ifdef PLATFORM_WINDOWS
	std::cout << "(00:00:00):" << msg << std::endl;
	#else
	std::cout << "\033[1;32m[](00:00:00):\033[0m" << msg << std::endl;
	#endif // PLATFORM_WINDOWS
}



/*Standard print function, prints into log if exist, else into stdout
* To create log, use Utils::NewLogger()
* To close logger, use Utils::ClodeLogger
*/
void Utils::PrintOutGL(std::string msg) {

	#ifdef PLATFORM_WINDOWS
	std::cout << "(00:00:00)[GRAPHIC_API]:" << msg << std::endl;
	#else
	std::cout << "\033[1;35m[](00:00:00)[GRAPHIC_API]:\033[0m" << msg << std::endl;
	#endif // PLATFORM_WINDOWS
}


/*Returns list of files in specified dir*/
void Utils::ListDir(std::string path, std::vector<std::string> result) {

	tinydir_dir dir;
	if (tinydir_open_sorted(&dir, path.c_str()) == -1) {
		return;
	}

	for (int i = 0; i < dir.n_files; i++) {
		tinydir_file file;
		if (tinydir_readfile_n(&dir, &file, i) == -1) {
			continue;
		}

		string filename = file.name;
		if (filename.length() < 3)
			continue;

		result.push_back(file.name);

	}
	tinydir_close(&dir);
}


/*Aligns memory number to be multiple of 12*/
unsigned int Utils::AlignMemory(unsigned int memory) {
	
	unsigned int req_mem_expansion = memory % 12;
	if (req_mem_expansion == 0) return memory;
	return memory += 12 - req_mem_expansion;
}


/*Vector to string */
std::string Utils::VecToStr(std::vector<float>& vector) {

	std::string retVal = "[";
	for (int i = 0; i < vector.size(); ++i) {
		retVal += std::to_string(vector[i]) + ",";
	}
	retVal += "]";
	return retVal;
}
